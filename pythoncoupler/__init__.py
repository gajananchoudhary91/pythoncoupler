#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# pythoncoupler - Software for coupling hydrodynamic and hydrologic software
#------------------------------------------------------------------------------#
"""
The main pythoncoupler module.

Contains the main pythoncoupler function imported as "main".
"""

from __future__ import absolute_import, print_function

if __name__ == '__main__':
    import pythoncoupler_path as _pythoncoupler_path
else:
    from . import pythoncoupler_path as _pythoncoupler_path

from pythoncoupler.main import main

__all__ = ['main', ]

############################################################################################################################################################
if (__name__ == '__main__'):
    pass

