#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# pythoncoupler - Software for coupling hydrodynamic and hydrologic software
#------------------------------------------------------------------------------#
'''Module calling pythoncoupler's main().'''

from __future__ import absolute_import

if (__package__ == "pythoncoupler"):
    from . import pythoncoupler_path as _pythoncoupler_path
    from .main import main
elif (__name__ == "__main__"):
    import pythoncoupler_path as _pythoncoupler_path
    from pythoncoupler.main import main
else:
    from . import pythoncoupler_path as _pythoncoupler_path
    from .main import main

################################################################################
if __name__ == '__main__':
    main()

