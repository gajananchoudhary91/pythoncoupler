#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#ifdef _MPI
#include <mpi.h>
#endif

using namespace std;

#define NGRIDS  2
#define NDIMS   3
#define NNODES  4
#define ARRAYLENGTH 2

enum CARD{
    NONE= 0,
    NB  = 1,
    VEL = 2,
    OTW = 3,
    DB  = 4
};

typedef struct{
    int ndims;
    int nnodes;
    double *xyz[NDIMS];
    enum CARD mycard;
} SGRID;

typedef struct{
    int ndims;
    int ngrids;
    int arraylength;
    double mydouble;
    int *myintarray;
    SGRID *grid;
    double *mydoublearray;
#ifdef _MPI
    int npes;
    int myid;
    MPI_Comm ADH_COMM;
#endif
}SMODEL;

extern "C"{
void print_address_size_align(const char *str, void *address, size_t size, size_t align);
void smodel_printsizesaligns(SMODEL *mod);
int  sgrid_alloc(SGRID **grid_ptr, int ngrids, int nnodes, int ndims);
void sgrid_free(SGRID *grid, int ngrids);
int  smodel_alloc(SMODEL **mod_ptr);
void smodel_defaults(SMODEL *mod);
int  smodel_free(SMODEL *mod);
int  adh_initialize(SMODEL **mod_ptr);
int  adh_run(SMODEL *mod);
int  adh_finalize(SMODEL *mod);
} //extern "C"{


/**************************************************************************************************/
/**************************************************************************************************/
void print_address_size_align(const char *str, void *address, size_t size, size_t align){
   cout << str << ": address = " << address <<", sizeof = " << size << ", alignof = " << align << endl;
}

/********************************************/
void smodel_printsizesaligns(SMODEL *mod){
    int i;
#ifdef _MPI
    int pe;
    for (pe = 0; pe < mod->npes; pe++){
        if(mod->myid == pe){
            cout << "Processor [" << (mod->myid+1) << "/" << mod->npes << "]: ";
#endif
            cout << "Inside smodel_printsizealigns(mod):" << endl;
            print_address_size_align(" *mod                ", &(*mod               ), sizeof(*mod               ), alignof(*mod               ));
            print_address_size_align("  mod->ndims         ", &( mod->ndims        ), sizeof( mod->ndims        ), alignof( mod->ndims        ));
            print_address_size_align("  mod->ngrids        ", &( mod->ngrids       ), sizeof( mod->ngrids       ), alignof( mod->ngrids       ));
            print_address_size_align("  mod->arraylength   ", &( mod->arraylength  ), sizeof( mod->arraylength  ), alignof( mod->arraylength  ));
            print_address_size_align("  mod->mydouble      ", &( mod->mydouble     ), sizeof( mod->mydouble     ), alignof( mod->mydouble     ));
            print_address_size_align("  mod->myintarray    ", &( mod->myintarray   ), sizeof( mod->myintarray   ), alignof( mod->myintarray   ));
            print_address_size_align("  mod->grid          ", &( mod->grid         ), sizeof( mod->grid         ), alignof( mod->grid         ));
            print_address_size_align("  mod->mydoublearray ", &( mod->mydoublearray), sizeof( mod->mydoublearray), alignof( mod->mydoublearray));
            for (i=0; i<mod->ngrids; i++){
                cout <<                  "       grid["<<i<<"]       ";
                print_address_size_align(""                     , &( mod->grid[i]        ), sizeof(mod->grid[i]         ), alignof( mod->grid[i]        ));
                print_address_size_align("       grid->ndims   ", &( mod->grid[i].ndims  ), sizeof( mod->grid[i].ndims  ), alignof( mod->grid[i].ndims  ));
                print_address_size_align("       grid->nnodes  ", &( mod->grid[i].nnodes ), sizeof( mod->grid[i].nnodes ), alignof( mod->grid[i].nnodes ));
                print_address_size_align("       grid->xyz     ", &( mod->grid[i].xyz    ), sizeof( mod->grid[i].xyz    ), alignof( mod->grid[i].xyz    ));
                print_address_size_align("       grid->xyz[0]  ", &( mod->grid[i].xyz[0] ), sizeof( mod->grid[i].xyz[0] ), alignof( mod->grid[i].xyz[0] ));
                print_address_size_align("       grid->xyz[1]  ", &( mod->grid[i].xyz[1] ), sizeof( mod->grid[i].xyz[1] ), alignof( mod->grid[i].xyz[1] ));
                print_address_size_align("       grid->xyz[2]  ", &( mod->grid[i].xyz[2] ), sizeof( mod->grid[i].xyz[2] ), alignof( mod->grid[i].xyz[2] ));
                print_address_size_align("       grid->mycard  ", &( mod->grid[i].mycard ), sizeof( mod->grid[i].mycard ), alignof( mod->grid[i].mycard ));
            }
#ifdef _MPI
            print_address_size_align("  mod->npes          ", &( mod->npes         ), sizeof( mod->npes         ), alignof( mod->npes         ));
            print_address_size_align("  mod->myid          ", &( mod->myid         ), sizeof( mod->myid         ), alignof( mod->myid         ));
            print_address_size_align("  mod->ADH_COMM      ", &( mod->ADH_COMM     ), sizeof( mod->ADH_COMM     ), alignof( mod->ADH_COMM     ));
        }
        fflush(stdout);
        MPI_Barrier(mod->ADH_COMM);
    }
#endif
}

/**************************************************************************************************/
/**************************************************************************************************/
int sgrid_alloc(SGRID **grid_ptr, int ngrids, int nnodes, int ndims){
    int i, j;
    *grid_ptr = (SGRID *) malloc(sizeof(SGRID) * ngrids);
    if (*grid_ptr == NULL) return 0;
    SGRID *grid = *grid_ptr;
    for (i=0; i<ngrids; i++) {
        grid[i].ndims = ndims;
        grid[i].nnodes = nnodes;
        for (j=0; j<grid[i].ndims; j++){
            grid[i].xyz[j] = (double *) malloc(sizeof(double)*nnodes);
            if (grid[i].xyz[j] == NULL) return 0;
        }
    }
    return 1;
}

/********************************************/
void sgrid_free(SGRID *grid, int ngrids){
    int i, j;
    for (i=0; i<ngrids; i++) {
        for (j=0; j<grid[i].ndims; j++){
            free(grid[i].xyz[j]);
            grid[i].xyz[j]=NULL;
        }
    }
}

/**************************************************************************************************/
/**************************************************************************************************/

int smodel_alloc(SMODEL **mod_ptr){
    cout << "This is the value of address smodel_alloc(...) that C got from Python, i.e., modptr." << endl;
    print_address_size_align(" *mod_ptr            ", mod_ptr, sizeof(*mod_ptr), alignof(*mod_ptr));
    *mod_ptr = (SMODEL *) malloc(sizeof(SMODEL));
    if (*mod_ptr==NULL) return 0;

    SMODEL *mod = *mod_ptr;
    mod->ndims = NDIMS;
    mod->ngrids = NGRIDS;
    mod->arraylength = ARRAYLENGTH;
    mod->myintarray = (int *) malloc(sizeof(int)*mod->arraylength);
    if (mod->myintarray == NULL) return 0;
    int check = sgrid_alloc(&(mod->grid), mod->ngrids, NNODES, mod->ndims);
    if (check == 0) return 0;
    mod->mydoublearray = (double *) malloc(sizeof(double)*mod->arraylength);
    if (mod->mydoublearray == NULL) return 0;

    return 1;
}

/********************************************/
void smodel_defaults(SMODEL *mod){
    int j, k, l;
    for (j=0; j<mod->arraylength; j++) mod->myintarray[j] = 0;
    for (j=0; j<mod->ngrids; j++){
        for (k=0; k<mod->grid[j].nnodes; k++){
            for (l=0; l<mod->ndims; l++){
                mod->grid[j].xyz[l][k] = 0.0;
            }
        }
        mod->grid[j].mycard = NONE;
    }
    for (j=0; j<mod->arraylength; j++) mod->mydoublearray[j] = 0.0;
#ifdef _MPI
    MPI_Comm_dup(MPI_COMM_WORLD, &(mod->ADH_COMM));

    // Get the number of processes
    MPI_Comm_size(mod->ADH_COMM, &(mod->npes));
    // Get the rank of the process
    MPI_Comm_rank(mod->ADH_COMM, &(mod->myid));
#endif

}

/********************************************/
int smodel_free(SMODEL *mod){
    free(mod->myintarray);
    sgrid_free(mod->grid, mod->ngrids);
    free(mod->grid);
    free(mod->mydoublearray);
    mod->myintarray=NULL;
    mod->grid=NULL;
    mod->mydoublearray=NULL;
#ifdef _MPI
    MPI_Comm_free(&(mod->ADH_COMM));
#endif
    free(mod);
    mod=NULL;
    return 1;
}

/**************************************************************************************************/
/**************************************************************************************************/
int adh_initialize(SMODEL **mod_ptr){
#ifdef _MPI
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);
#endif

    cout << "\nInitializing model...\n";
    smodel_alloc(mod_ptr);
    SMODEL *mod = *mod_ptr;
    smodel_defaults(mod);
#ifdef _MPI
    int pe;
    for (pe = 0; pe < mod->npes; pe++){
        if(mod->myid == pe){
            cout << "Processor [" << (mod->myid+1) << "/" << mod->npes << "]: ";
#endif
            cout << "C Allocated model memory for *mod_ptr in function smodel_alloc(**modptr):" << endl;
            //print_address_size_align(" *mod_ptr            ",  mod_ptr, sizeof( *mod_ptr), alignof( *mod_ptr));
            print_address_size_align("**mod_ptr            ", *mod_ptr, sizeof(**mod_ptr), alignof(**mod_ptr));
#ifdef _MPI
        }
        fflush(stdout);
        MPI_Barrier(mod->ADH_COMM);
    }
#endif
    smodel_printsizesaligns(mod);
    return 1;
}

/********************************************/
int adh_run(SMODEL *mod){
    /*Do nothing for now*/
    cout << "\nRunning SW" << mod->ndims << "D model...\n";
    return 1;
}

/********************************************/
int adh_finalize(SMODEL *mod){

    cout << "\nFinalizing SW" << mod->ndims << "D model...\n";
    int status = smodel_free(mod);
    if (status == 0) {
#ifdef _MPI
        MPI_Abort(mod->ADH_COMM, -1);
#else
        exit(-1);
#endif
    }

#ifdef _MPI
    // Finalize the MPI environment.
    MPI_Finalize();
#endif
    return 1;
}
