import ctypes, sys

foolib = ctypes.cdll.LoadLibrary('./libfoo.so')

class SGRID(ctypes.Structure):
     _fields_ = [("ndims", ctypes.c_int),
                 ("nnodes", ctypes.c_int),
                 ("xyz", 3*ctypes.POINTER(ctypes.c_double)),
                 ("mycard", ctypes.c_int)]
     def __init__(self):
         self.ndims = 3
         self.nnodes = 4
         for i in range(self.ndims):
             self.xyz[i] = (ctypes.c_double *self.nnodes)()
         self.mycard = 421

class SMODEL(ctypes.Structure):
     _fields_ = [("ndims", ctypes.c_int),
                 ("ngrids", ctypes.c_int),
                 ("arraylength", ctypes.c_int),
                 ("mydouble", ctypes.c_double),
                 ("myintarray", ctypes.POINTER(ctypes.c_int)),
                 ("grid", ctypes.POINTER(SGRID)),
                 ("mydoublearray", ctypes.POINTER(ctypes.c_double)),
                 ("npes", ctypes.c_int),
                 ("myid", ctypes.c_int),
                 ("ADH_COMM", ctypes.c_void_p)]
     def __init__(self):
         self.ndims = 3
         self.ngrids = 1
         self.arraylength = 2
         self.mydouble = 0.0
         self.myintarray = (ctypes.c_int * self.arraylength)()
         tempgrid = SGRID()
         self.grid = ctypes.pointer(tempgrid)
         self.mydoublearray = (ctypes.c_double * self.arraylength)()
         self.npes = 0
         self.myid = 0
         self.ADH_COMM = None


def main():
    foolib.adh_initialize.restype = ctypes.c_int
    foolib.adh_initialize.argtypes = [ctypes.POINTER(ctypes.POINTER(SMODEL))]
    foolib.smodel_printsizesaligns.restype = None
    foolib.smodel_printsizesaligns.argtypes = [ctypes.POINTER(SMODEL)]
    foolib.adh_finalize.restype = ctypes.c_int
    foolib.adh_finalize.argtypes = [ctypes.POINTER(SMODEL)]

    print "/***************************************************/"
    print "Initializing program"
    mod = ctypes.POINTER(SMODEL)()
    status = foolib.adh_initialize(mod);
    if (status == 0): print "\n\n\nFailure in allocating model in C.\n\n\n"
    print "This is the value of address, i.e., 'byref(mod)' that should be passed to smodel_alloc(...) by Python."
    print "ctypes.addressof(mod)          =", hex(ctypes.addressof(mod))

    print "/***************************************************/"
    print "How Python sees the memory allocated in C:"
    print "Calling smodel_printsizealigns(mod) directly through Python:"
    foolib.smodel_printsizesaligns(mod)
    print "\nPrinting data stored inside Python:"
    print "mod should be equivalent to C type (SMODEL *)"
    print "ctypes.addressof(mod)          =", hex(ctypes.addressof(mod))
    print "                 mod           =", mod
    print "ctypes.addressof(mod.contents) =", hex(ctypes.addressof(mod.contents))
    print "                 mod.contents  =", mod.contents
    print "Printing addresses stored inside Python, through hex(ctypes.addressof(...)):"
    print "  mod                : address =", hex(ctypes.addressof( mod                        ))
    print " *mod                : address =", hex(ctypes.addressof( mod[0]               ))
    #print "  mod->ndims         : address =", hex(ctypes.addressof( mod[0].ndims         )) #Error for some reason. Only pointers seem to be fine.
    #print "  mod->ngrids        : address =", hex(ctypes.addressof( mod[0].ngrids        ))
    #print "  mod->arraylength   : address =", hex(ctypes.addressof( mod[0].arraylength   ))
    #print "  mod->mydouble      : address =", hex(ctypes.addressof( mod[0].mydouble      ))
    print "  mod->myintarray    : address =", hex(ctypes.addressof( mod[0].myintarray    ))
    print "  mod->grid          : address =", hex(ctypes.addressof( mod[0].grid          ))
    print "  mod->mydoublearray : address =", hex(ctypes.addressof( mod[0].mydoublearray ))
    for i in range(mod[0].ngrids):
        print "       grid["+str(i)+"]       : address =", hex(ctypes.addressof( mod[0].grid[i]        ))
        #print "       grid["+str(i)+"].ndims : address =", hex(ctypes.addressof( mod[0].grid[i].ndims  ))
        #print "       grid["+str(i)+"].nnodes: address =", hex(ctypes.addressof( mod[0].grid[i].nnodes ))
        print "       grid["+str(i)+"].xyz   : address =", hex(ctypes.addressof( mod[0].grid[i].xyz    ))
        print "       grid["+str(i)+"].xyz[0]: address =", hex(ctypes.addressof( mod[0].grid[i].xyz[0] ))
        print "       grid["+str(i)+"].xyz[1]: address =", hex(ctypes.addressof( mod[0].grid[i].xyz[1] ))
        print "       grid["+str(i)+"].xyz[2]: address =", hex(ctypes.addressof( mod[0].grid[i].xyz[2] ))
        #print "       grid["+str(i)+"].mycard: address =", hex(ctypes.addressof( mod[0].grid[i].mycard ))

    print "/***************************************************/"
    print "/***************************************************/"
    print "/***************************************************/"
    print "/***************************************************/"
    print "Creating a new model in Python to pass to C:"
    newmod = SMODEL()
    foolib.smodel_defaults(ctypes.byref(newmod));
    #newmod.grid = ctypes.pointer(tempgrid)
    #data0 = [112.4, 24.3];
    #data1 = [ 10.4,  5.3];
    #data2 = [ -2.4,  1.3];
    #tempgrid.xyz[0] = (ctypes.c_double *2)(*data0);
    #tempgrid.xyz[1] = (ctypes.c_double *2)(*data1);
    #tempgrid.xyz[2] = (ctypes.c_double *2)(*data2);
    print "ctypes.addressof(newmod)          =", hex(ctypes.addressof(mod))
    print "                 newmod           =", mod
    print "ctypes.addressof(newmod.contents) =", hex(ctypes.addressof(mod.contents))
    print "                 newmod.contents  =", mod.contents
    print "Calling smodel_printsizealigns(newmod) directly through Python:"
    foolib.smodel_printsizesaligns(ctypes.byref(newmod));

    print "/***************************************************/"
    print "Freeing values\n"
    status = foolib.adh_finalize(mod);
    if (status == 1): print "Success"
    if (status == 0): print "Failure"

if __name__ == "__main__":
    main()
