#!/usr/bin/python
import ctypes, sys

adhlib = ctypes.cdll.LoadLibrary('./libadh.so')

class SGRID(ctypes.Structure):
     _fields_ = [("ndims", ctypes.c_int),
                 ("nnodes", ctypes.c_int),
                 ("xyz", 3*ctypes.POINTER(ctypes.c_double))]

class SMODEL(ctypes.Structure):
     _fields_ = [("ndims", ctypes.c_int),
                 ("ngrids", ctypes.c_int),
                 ("arraylength", ctypes.c_int),
                 ("mydouble", ctypes.c_double),
                 ("myintarray", ctypes.POINTER(ctypes.c_int)),
                 ("grid", ctypes.POINTER(SGRID)),
                 ("mydoublearray", ctypes.POINTER(ctypes.c_double))]

def main():
    print "/***************************************************/"
    print "Allocating and initializing values"
    mod = ctypes.POINTER(SMODEL)()
    print "This is the value of address, i.e., 'byref(mod)' that should be passed to smodel_alloc(...) by Python."
    print "ctypes.addressof(mod)          =", hex(ctypes.addressof(mod))

    adhlib.smodel_alloc.restype = ctypes.c_int
    adhlib.smodel_alloc.argtypes = [ctypes.POINTER(ctypes.POINTER(SMODEL))]
    status = adhlib.smodel_alloc(ctypes.byref(mod))
    if (status == 0): print "\n\n\nFailure in allocating model in C.\n\n\n"
    adhlib.smodel_defaults(ctypes.byref(mod))
    print "/***************************************************/"
    print "How Python sees the memory allocated in C:"
    print "Calling smodel_printsizealigns(mod) directly through Python:"
    adhlib.smodel_printsizesaligns(mod);
    print "\nPrinting data stored inside Python:"
    print "mod should be equivalent to C type (SMODEL *)"
    print "ctypes.addressof(mod)          =", hex(ctypes.addressof(mod))
    print "                 mod           =", mod
    print "ctypes.addressof(mod.contents) =", hex(ctypes.addressof(mod.contents))
    print "                 mod.contents  =", mod.contents
    print "Printing addresses stored inside Python, through hex(ctypes.addressof(...)):"
    print "  mod                : address =", hex(ctypes.addressof( mod                        ))
    print " *mod                : address =", hex(ctypes.addressof( mod[0]               ))
    #print "  mod->ndims         : address =", hex(ctypes.addressof( mod[0].ndims         ))
    #print "  mod->ngrids        : address =", hex(ctypes.addressof( mod[0].ngrids        ))
    #print "  mod->arraylength   : address =", hex(ctypes.addressof( mod[0].arraylength   ))
    #print "  mod->mydouble      : address =", hex(ctypes.addressof( mod[0].mydouble      ))
    print "  mod->myintarray    : address =", hex(ctypes.addressof( mod[0].myintarray    ))
    print "  mod->grid          : address =", hex(ctypes.addressof( mod[0].grid          ))
    print "  mod->mydoublearray : address =", hex(ctypes.addressof( mod[0].mydoublearray ))
    for i in range(mod[0].ngrids):
        print "  mod->grid["+str(i)+"]       : address =", hex(ctypes.addressof( mod[0].grid[i]        ))
        #print "       grid["+str(i)+"].ndims : address =", hex(ctypes.addressof( mod[0].grid[i].ndims  ))
        #print "       grid["+str(i)+"].nnodes: address =", hex(ctypes.addressof( mod[0].grid[i].nnodes ))
        print "       grid["+str(i)+"].xyz   : address =", hex(ctypes.addressof( mod[0].grid[i].xyz    ))
        print "       grid["+str(i)+"].xyz[0]: address =", hex(ctypes.addressof( mod[0].grid[i].xyz[0] ))
        print "       grid["+str(i)+"].xyz[1]: address =", hex(ctypes.addressof( mod[0].grid[i].xyz[1] ))
        print "       grid["+str(i)+"].xyz[2]: address =", hex(ctypes.addressof( mod[0].grid[i].xyz[2] ))

    print "/***************************************************/"
    print "Freeing values\n"
    adhlib.smodel_free.restype = ctypes.c_int
    adhlib.smodel_free.argtypes = [ctypes.POINTER(SMODEL)]
    status = adhlib.smodel_free(mod)
    if (status == 1): print "Success"
    if (status == 0): print "Failure"

if __name__ == "__main__":
    main()
