#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

#define LENGTH 2
#define ARRAYLENGTH 3

/**********************************************************************************************************/
/**********************************************************************************************************/
template <class _TYPE>
class MYARRAY {
    public:
        int length;                               //Variable
        _TYPE *value;                             //Function

        void myfreemembers_myarray();             //Function
        _TYPE mysum_myarray();                    //Function

    private:
        _TYPE mysum;                              //Variable
};

class MYCLASS {
    public:
        int myint;                                //Variable
        MYARRAY<double> myarray;                  //Variable

        MYCLASS();                                //Function:Constructor
        void mysetmydouble_myclass(double x);     //Function
        double mygetmydouble_myclass();           //Function
        void myprint_myclass();                   //Function

    private:
        double mydouble;                          //Variable
};
 
typedef struct{
    int newint;                                   //Variable
    MYCLASS *data;                                //Variable
    double newdouble;                             //Variable
} MYSTRUCT;

extern "C"{
    void myalloc_mystruct(MYSTRUCT **mystruct);
    void myfree_mystruct(MYSTRUCT *mystruct);
    void myprint_mystruct(MYSTRUCT *mystruct);
    void python_mysetmydouble_myclass(MYCLASS *myclass,double x){
        myclass->mysetmydouble_myclass(x);
    }
    double python_mygetmydouble_myclass(MYCLASS *myclass){
        return myclass->mygetmydouble_myclass();
    }
}

/**********************************************************************************************************/
/**********************************************************************************************************/
template <class _TYPE>
void MYARRAY<_TYPE>::myfreemembers_myarray(){
    delete [] value;
}

/****************************************************************/
template <class _TYPE>
_TYPE MYARRAY<_TYPE>::mysum_myarray(){
    int i;
    _TYPE mysum = (_TYPE) 0;
    for (i=0; i<length; i++) mysum += value[i];
    return mysum;
}

/**********************************************************************************************************/
/**********************************************************************************************************/
MYCLASS::MYCLASS(){
    myint = 1;
    mysetmydouble_myclass(51.0);
}

/****************************************************************/
void MYCLASS::mysetmydouble_myclass(double x){
    mydouble = x;
}

/****************************************************************/
double MYCLASS::mygetmydouble_myclass(){
    return mydouble;
}

/****************************************************************/
void MYCLASS::myprint_myclass(){
    int i;
    printf("\tMy C class:\n\t\tmyint = %i\n\t\tmydouble = %f\n", myint, mygetmydouble_myclass());
    printf("\t\tmyarray.length = %i, myarray.value = [", myarray.length);
    for (i=0; i<myarray.length; i++){
        printf(" %f", myarray.value[i]);
    }
    printf("];");
    cout << " sum = " << myarray.mysum_myarray() << endl;
}

/**********************************************************************************************************/
/**********************************************************************************************************/
void myalloc_mystruct(MYSTRUCT **mystruct){
    *mystruct = new MYSTRUCT [LENGTH];
}

/****************************************************************/
void myfree_mystruct(MYSTRUCT *mystruct){
    int i;
    for (i=0; i<LENGTH; i++) delete [] mystruct->data[i].myarray.value;
    delete [] mystruct->data;
}

/****************************************************************/
void myprint_mystruct(MYSTRUCT *mystruct){
    printf("\n");
    int i;
    printf("--------------------------------\n");
    printf("My C struct:\n\tnewint = %i\n\tnewdouble = %f\n", mystruct->newint, mystruct->newdouble);
    for (i=0; i<LENGTH; i++)
        mystruct->data[i].myprint_myclass();
}

/****************************************************************/
extern "C" void myalloc_myclass(MYCLASS **myclassptr){
    int i, j;
    *myclassptr = new MYCLASS [LENGTH];
    for (i=0; i<LENGTH; i++){
        (*myclassptr)[i].myint = 11;
        (*myclassptr)[i].mysetmydouble_myclass(52.0);
        (*myclassptr)[i].myarray.length = ARRAYLENGTH;
        (*myclassptr)[i].myarray.value = new double[ARRAYLENGTH];
        for (j=0; j<ARRAYLENGTH; j++){
            (*myclassptr)[i].myarray.value[j] = (double)((i+1)*j);
        }
        (*myclassptr)[i].myprint_myclass();
    }
}

/**********************************************************************************************************/
/**********************************************************************************************************/
