#!/usr/bin/env python
import ctypes;
DEBUG=1


class foo(ctypes.Structure):
    pass

from bar_h import *

list1=[("barptr", ctypes.POINTER(bar))]
list2=[("myint",ctypes.c_int)]
if DEBUG==1:
    foo._fields_=list1+list2
else:
    foo._fields_=list1

class foobar(ctypes.Structure):
    _fields_ = [("myint1", ctypes.c_int)]
    _fields_.extend([("myint2",ctypes.c_int)])

class barfoo(ctypes.Structure):
    pass
list1=[("myint1", ctypes.c_int)]
list2=[("myint2",ctypes.c_int),("myint3",ctypes.c_int)]


print [list1[:], list2[:]]
print [list1[:]]


if DEBUG==2:
    barfoo._fields_ = list1+ list2
else:
    barfoo._fields_ = list1



