#!/usr/bin/python
import ctypes
import numpy, sys


#gsshalib = ctypes.cdll.LoadLibrary('~/Desktop/Research/adh-gssha-ukmet/pythoncoupler/libgssha.so')
gsshalib = ctypes.cdll.LoadLibrary('./libgssha.so')

#def wrap_function(lib, funcname, restype, argtypes):
#    """Simplify wrapping ctypes functions"""
#    func = lib.__getattr__(funcname)
#    func.restype = restype
#    func.argtypes = argtypes
#    return func

def mygetmydouble_myclass(self):
    gsshalib.python_mygetmydouble_myclass.restype  = ctypes.c_double
    gsshalib.python_mygetmydouble_myclass.argtypes = [ctypes.c_void_p]
    return gsshalib.python_mygetmydouble_myclass(ctypes.byref(self))

def mysetmydouble_myclass(self, val):
    gsshalib.python_mysetmydouble_myclass.restype  = None
    gsshalib.python_mysetmydouble_myclass.argtypes = [ctypes.c_void_p, ctypes.c_double]
    gsshalib.python_mysetmydouble_myclass(ctypes.byref(self),val)

class MYARRAY(ctypes.Structure):
    _fields_ = [("length", ctypes.c_int),
                ("value", ctypes.POINTER(ctypes.c_double)),
                ("mysum", ctypes.c_double)]

class MYCLASS(ctypes.Structure):
    pass

MYCLASS._pack_ = 8
MYCLASS._fields_ = [("myint", ctypes.c_int),
                    ("myarray", MYARRAY),
                    ("mydouble", ctypes.c_double)]
MYCLASS.mygetmydouble_myclass = mygetmydouble_myclass
MYCLASS.mysetmydouble_myclass = mysetmydouble_myclass


class MYSTRUCT(ctypes.Structure):
    _fields_ = [("newint", ctypes.c_int),
                ("myclass", ctypes.POINTER(MYCLASS)),
                ("newdouble", ctypes.c_double)]

    def myprint_mystruct(self):
        print "--------------------------------"
        print "My Python struct:"
        print "\tnewint = ", self.newint
        print "\tnewdouble = ", self.newdouble
        for i in range(2):
            print "\tMy Python class:"
            print "\t\tmyint = ", self.myclass[i].myint
            print "\t\tmydouble = ", self.myclass[i].mydouble
            print "\t\tmyarray.length = ", self.myclass[i].myarray.length, "myarray.value = [ ",
            for j in range(self.myclass[i].myarray.length):
                print self.myclass[i].myarray.value[j],
            print "]"



def main():
    print "/***************************************************/"
    print "Allocating and initializing values"
    mystruct = MYSTRUCT()
    mystruct.newint = 15
    mystruct.newdouble = 23512.51
    gsshalib.myalloc_myclass(ctypes.byref(mystruct.myclass))
    gsshalib.myprint_mystruct(ctypes.byref(mystruct))
    mystruct.myprint_mystruct()

    print "/***************************************************/"
    print "Changing values\n"
    mystruct.newdouble = -810.0
    mystruct.newint = -861416812
    mystruct.myclass[0].myint = 21
    mystruct.myclass[1].myint = 941
    gsshalib.myprint_mystruct(ctypes.byref(mystruct))
    mystruct.myprint_mystruct()

    mystruct.myclass[0].mysetmydouble_myclass(251.0)
    gsshalib.myprint_mystruct(ctypes.byref(mystruct))
    mystruct.myprint_mystruct()
    mystruct.myclass[1].mysetmydouble_myclass(6.0)
    gsshalib.myprint_mystruct(ctypes.byref(mystruct))
    mystruct.myprint_mystruct()

    print mystruct.myclass[0].mygetmydouble_myclass()
    print mystruct.myclass[1].mygetmydouble_myclass()

    print "\n/***************************************************/"
    print "Freeing values\n"
    gsshalib.myfree_mystruct(ctypes.byref(mystruct))
    gsshalib.myprint_mystruct(ctypes.byref(mystruct))
    mystruct.myprint_mystruct()

if __name__ == "__main__":
    main()
