#!/usr/bin/env python3
from __future__ import absolute_import, print_function
import ctypes as ct

############################################################################################################################################################
import gsshapython.sclass.build_options   as gsshaopts
import gsshapython.sclass.define_h        as gsshadefine
import gsshapython.sclass.fnctn_h         as gsshafnctn
from gsshapython.sclass.timeseriesdefs_h import ts_struct

############################################################################################################################################################
import adhpython.sclass.build_options     as adhopts
import adhpython.sclass.define_h          as adhdefine
import adhpython.sclass.fnctn_h           as adhfnctn
import adhpython.sclass.smodel_h          as adhsmodel
import adhpython.sclass.sgrid_h           as adhsgrid
import adhpython.sclass.svect2d_h         as adhsvect2d

############################################################################################################################################################
############################################################################################################################################################
DEBUG_LOCAL = 1
############################################################################################################
def gssha_set_bc_from_adh_depths_discontinuous(ags): # ags is of type adhgsshatruct.

    from pythoncoupler.coupler.adhgsshastruct import SERIESLENGTH, TIME_TOL

    #Aliases
    mod=ct.POINTER(adhsmodel.SMODEL)()
    mod.contents = ags.sm[0].submodel[0]
    grid=ct.POINTER(adhsgrid.SGRID)()
    grid.contents = ags.sm[0].submodel[0].grid[0]

    assert(ags.mvs[0].yes_head_bound == 1)

    ######################################################
    #SET UP gssha BC from AdH.
    ######################################################
    max_depth = 0.0

    for ie in range(grid[0].nelems1d):
        if (grid[0].elem1d[ie].string == ags.adhedgestringid):
            n1 = grid[0].elem1d[ie].nodes[0]
            n2 = grid[0].elem1d[ie].nodes[1]
            h1 = mod[0].sw[0].d2[0].head[n1]
            h2 = mod[0].sw[0].d2[0].head[n2]
            v1 = mod[0].sw[0].d2[0].vel[n1]
            v2 = mod[0].sw[0].d2[0].vel[n2]
            vavg = adhsvect2d.SVECT2D((v1.x + v2.x)/2.0, (v1.y + v2.y)/2.0)
            if (grid[0].node[n1].resident_pe == grid[0].smpi[0].myid):
                max_depth = max(max_depth, h1)
            if (grid[0].node[n2].resident_pe == grid[0].smpi[0].myid):
                max_depth = max(max_depth, h2)
            print("PE[",ags.myid,"] Edge avg. speed =", adhfnctn.svect2d_mag(vavg))
    if (adhopts._MESSG==adhdefine.ON):
        max_depth = adhfnctn.messg_dmax(ct.c_double(max_depth), grid[0].smpi[0].ADH_COMM)

    if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0:
        print("PE[",ags.myid,"] Edge string(",ags.adhedgestringid,"): Maximum depth = ", max_depth)
        print("PE[",ags.myid,"] GSSHA specified outlet depth 'BOUND_DEPTH' = ", ags.mvs[0].boundary_depth)
        print("PE[",ags.myid,"] Resetting BOUND_DEPTH to ", max_depth)

    ags.mvs[0].boundary_depth = max_depth

############################################################################################################
def gssha_set_bc_from_adh_depths(ags): # ags is of type adhgsshatruct.

    from pythoncoupler.coupler.adhgsshastruct import SERIESLENGTH, TIME_TOL

    #Aliases
    mod=ct.POINTER(adhsmodel.SMODEL)()
    mod.contents = ags.sm[0].submodel[0]
    grid=ct.POINTER(adhsgrid.SGRID)()
    grid.contents = ags.sm[0].submodel[0].grid[0]

    assert(ags.mvs[0].yes_head_bound == 1)
    assert(ags.mvs[0].bound_ts == 1)
    assert(ags.mvs[0].bound_ts_ptr[0].num_vals > 3)
    ts = ags.mvs[0].bound_ts_ptr[0]

    if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) and DEBUG_LOCAL != 0 and ags.myid == 0:
        print()
        for i in range(ts.num_vals):
            print('Before:(t,v)[',i,'] = (', ts.jul_time[i],',', ts.val[i],')')

    ######################################################
    #SET UP gssha BC from AdH.
    ######################################################
    # Find the value of maximum depth first.
    if (ags.adhrunflag != adhdefine.OFF):
        max_delta_h = -1.0e+200
        min_delta_h =  1.0e+200
        count=0.0
        delta_h=0.0
        h=0.0
        for ie in range(grid[0].nelems1d):
            if (grid[0].elem1d[ie].string == ags.adhedgestringid):
                n1 = grid[0].elem1d[ie].nodes[0]
                n2 = grid[0].elem1d[ie].nodes[1]
                h1 = mod[0].sw[0].d2[0].head[n1]
                h2 = mod[0].sw[0].d2[0].head[n2]
                old_h1 = mod[0].sw[0].d2[0].old_head[n1]
                old_h2 = mod[0].sw[0].d2[0].old_head[n2]
                v1 = mod[0].sw[0].d2[0].vel[n1]
                v2 = mod[0].sw[0].d2[0].vel[n2]
                vavg = adhsvect2d.SVECT2D((v1.x + v2.x)/2.0, (v1.y + v2.y)/2.0)
                if (grid[0].node[n1].resident_pe == grid[0].smpi[0].myid):
                    h  += h1*grid[0].elem1d[ie].djac  # Average depth calculation
                    delta_h  += (h1-old_h1)*grid[0].elem1d[ie].djac
                    count    += grid[0].elem1d[ie].djac
                    max_delta_h = max(max_delta_h, h1-old_h1)
                    min_delta_h = min(min_delta_h, h1-old_h1)
                if (grid[0].node[n2].resident_pe == grid[0].smpi[0].myid):
                    h  += h2*grid[0].elem1d[ie].djac  # Average depth calculation
                    delta_h  += (h2-old_h2)*grid[0].elem1d[ie].djac
                    count    += grid[0].elem1d[ie].djac
                    max_delta_h = max(max_delta_h, h2-old_h2)
                    min_delta_h = min(min_delta_h, h2-old_h2)
                print("PE[",ags.myid,"] Edge avg. speed =", adhfnctn.svect2d_mag(vavg))

        # Note: Hoping whichever depth is closes to GSSHA current depth works better in damping oscillations than dmax alone!
        if (adhopts._MESSG==adhdefine.ON):
            h         = adhfnctn.messg_dsum(ct.c_double(h)        , grid[0].smpi[0].ADH_COMM)
            delta_h   = adhfnctn.messg_dsum(ct.c_double(delta_h)  , grid[0].smpi[0].ADH_COMM)
            count     = adhfnctn.messg_dsum(ct.c_double(count)    , grid[0].smpi[0].ADH_COMM)
            max_delta_h = adhfnctn.messg_dmax(ct.c_double(max_delta_h), grid[0].smpi[0].ADH_COMM)
            min_delta_h = adhfnctn.messg_dmin(ct.c_double(min_delta_h), grid[0].smpi[0].ADH_COMM)

        #avg_delta_h = delta_h/count

        avg_delta_h   = (h - ags.adh_hprev*ags.adh_hprev_len)/count
        ags.adh_hprev = h/count
        ags.adh_hprev_len = count

        if adhopts._DEBUG == adhdefine.ON or DEBUG_LOCAL != 0:
            #print("PE[",ags.myid,"] Edge string(",ags.adhedgestringid,"): Maximum delta_h = ", max_delta_h)
            #print("PE[",ags.myid,"] Edge string(",ags.adhedgestringid,"): Minimum delta_h = ", min_delta_h)
            print("PE[",ags.myid,"] Edge string(",ags.adhedgestringid,"): Average delta_h = ", avg_delta_h)

        if ags.couplingtype == 'gdadg':
            DT = 0.0
            #while (ags.mvs[0].niter*ags.gsshatimefact+DT < ags.sm[0].submodel[0].t_prev+ags.adhdt-TIME_TOL):
            #assert(ags.mvs[0].timer*ags.gsshatimefact -TIME_TOL < ags.sm[0].submodel[0].t_prev+ags.adhdt+TIME_TOL and ags.mvs[0].timer*ags.gsshatimefact + TIME_TOL> ags.sm[0].submodel[0].t_prev)
            while (ags.mvs[0].timer*ags.gsshatimefact + DT < ags.sm[0].submodel[0].t_prev+ags.adhdt-TIME_TOL):
                DT += ags.effectivegsshadt
        else: # For adg and adgda:
            pass
        #DT = 0.0
        #while (ags.mvs[0].niter*ags.gsshatimefact+DT < ags.sm[0].submodel[0].t_prev-ags.effectivegsshadt+TIME_TOL):
        #    DT += ags.effectivegsshadt
        #if ags.couplingtype == 'gdadg':
        #    DT += ags.effectivegsshadt
        #print(DT, DT/86400.0)

        # Shift the time series
        for i in range(ts.num_vals-1):
            ts.jul_time[i] = ts.jul_time[i+1]
            ts.val[i]      = ts.val[i+1]
        ts.last_access = ts.last_access-1

        # Add the new value of time.
        ts.jul_time[ts.num_vals-2] = ags.gsshatstartjul+(ags.sm[0].submodel[0].t_prev/86400.0) # Have to convert AdH current time to corresponding next GSSHA Julian time.

        ######################################################################################
        # Gajanan gkc. We need to decide what to use here. Stability is likely going to get
        # affected with this. Also important to consider that GSSHA and AdH may have
        # different values of depths at their interface.

        ## TYPE 1  -  This uses max/min depth.
        ## if abs(ts.val[ts.num_vals-2]-max_depth) < abs(ts.val[ts.num_vals-2]-min_depth):
        #if abs(max_delta_h) < abs(min_delta_h):
        #    delta_h = max_delta_h
        #else:
        #    delta_h = min_delta_h
        #ts.val[ts.num_vals-2] += delta_h

        # TYPE 2  -  This uses average change in depth.
        ts.val[ts.num_vals-2] += avg_delta_h

        ######################################################################################


        if ags.couplingtype == 'gdadg':
            #ts.jul_time[ts.num_vals-2] += max(ags.effectivegsshadt, ags.adhdt)/86400.0 #Julian
            ts.jul_time[ts.num_vals-2] = ags.mvs[0].btime + DT/86400.0 #Julian
        # For round of errors:
        ts.jul_time[ts.num_vals-1] = ts.jul_time[ts.num_vals-2] + (TIME_TOL/86400.0)
        ts.val[ts.num_vals-1]      = ts.val[ts.num_vals-2]
    else:

        for i in range (ts.num_vals-1):
            ts.jul_time[i] = ts.jul_time[i+1]
            ts.val[i]      = ts.val[i+1]
        ts.jul_time[ts.num_vals-1] += ts.jul_time[ts.num_vals-3]-ts.jul_time[ts.num_vals-4]

    if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) and DEBUG_LOCAL != 0 and ags.myid == 0:
        print('Current GSSHA julian time =', ags.mvs[0].btime)
        #assert(ags.mvs[0].btime <= ts.jul_time[ts.num_vals-2])
        #assert(ags.mvs[0].btime >= ts.jul_time[0])
        for i in range(ts.num_vals):
            print('After :(t,v)[',i,'] = (', ts.jul_time[i],',', ts.val[i],')')

############################################################################################################
if __name__ == '__main__':
    pass

