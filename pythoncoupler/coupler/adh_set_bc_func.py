#!/usr/bin/env python3
from __future__ import absolute_import, print_function
import ctypes

############################################################################################################################################################
import adhpython.sclass.build_options   as adhopts
import adhpython.sclass.define_h        as adhdefine
from adhpython.sclass.fnctn_h import messg_dmax

############################################################################################################################################################
import gsshapython.sclass.build_options as gsshaopts
import gsshapython.sclass.define_h      as gsshadefine

############################################################################################################################################################

############################################################################################################################################################
DEBUG_LOCAL = 1
############################################################################################################
def adh_set_bc_from_gssha_hydrograph(ags): # ags is of type adhgsshatruct.

    from pythoncoupler.coupler.adhgsshastruct import SERIESLENGTH, TIME_TOL

    ########## Set AdH Boundary Conditions ###########
    # Note: ags.adhseries already points to the head of series in AdH that needs to be modified.
    if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) and DEBUG_LOCAL != 0 and ags.myid == 0:
        print()
        for i in range(ags.adhseries[0].size):
            print('Before:(t,v)[',i,'] = (', ags.adhseries[0].entry[i].time,',',ags.adhseries[0].entry[i].value[0],')')

    if adhopts._MESSG == adhdefine.ON:
        if ags.myid != 0:
            messgvout  = -1.0E+200
        else:
            messgvout  = ags.mvs[0].vout
        if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) or DEBUG_LOCAL != 0:
            print('PE[',ags.myid,'] Before messg: vout = ', ags.mvs[0].vout)
        ags.mvs[0].vout  = messg_dmax(ctypes.c_double(messgvout), ags.sm[0].submodel[0].grid[0].smpi[0].ADH_COMM)
        if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) or DEBUG_LOCAL != 0:
            print('PE[',ags.myid,'] After messg : vout = ', ags.mvs[0].vout)

    if (ags.gssharunflag != gsshadefine.OFF):

        # Inflow volume in the current gssha time step:
        # V=(t2-t1)(q1+q2)/2; So to conserve mass entering in AdH in interval t2-t1, q2 = 2*V/(t2-t1) - q1  in cu.m/s
        # Therefore, in (cu.m/s)/m, AdH series value be val2 = 2*V/(t2-t1)/edgestringleng - val1; since val_i=q_i/edgestringlen
        DV = (ags.mvs[0].vout-ags.gsshavoutprev)
        if ags.couplingtype == 'adgda':
            DT = 0.0
            while (ags.sm[0].submodel[0].t_prev + DT < ags.mvs[0].timer*ags.gsshatimefact+ags.effectivegsshadt-TIME_TOL):
                DT += ags.adhdt
        else: # For gda and gdadg:
            DT = (ags.mvs[0].timer-ags.gsshavoutprev_t)*ags.gsshatimefact + 1.0E-20

        if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) and DEBUG_LOCAL != 0 and ags.myid == 0:
            # This is valid only for PE 0 which is running GSSHA. Not on other PEs!
            outlet_area = ags.mvs[0].area[ags.mvs[0].nx[ags.mvs[0].nlinks]][ags.mvs[0].nlinks]
            print("outlet area       =", outlet_area, "m2")
            print("qout              =", ags.mvs[0].qout, "m3/s")
            print("voutprev          =", ags.gsshavoutprev, "m3")
            print("vout              =", ags.mvs[0].vout, "m3")
            print("DV                =", DV, "m3")
            print("DT                =", DT, "s")
            print("GSSHA qout/area   =", ags.mvs[0].qout / outlet_area,"m/s")
            print("Expected AdH speed=", DV / DT / outlet_area, "m/s (needs more work!)")

        if DV < 0.0:
            print("Warning: Outflow from AdH model forced by GSSHA! This can cause instabilities in the model!")

        # Move current to previous: Current is at [2], previous is at [1]
        for i in range(ags.adhseries[0].size-1):
            ags.adhseries[0].entry[i].time     = ags.adhseries[0].entry[i+1].time
            ags.adhseries[0].entry[i].value[0] = ags.adhseries[0].entry[i+1].value[0]

        # Set AdH series value for gssha time t2
        ags.adhseries[0].entry[ags.adhseries[0].size-2].time     = ags.mvs[0].timer*ags.gsshatimefact # This is GSSHA time set in AdH series.
        if ags.couplingtype == 'adgda':
            ags.adhseries[0].entry[ags.adhseries[0].size-2].time = ags.sm[0].submodel[0].t_prev+DT #ags.adhdt # If 2-way adgda, then time series has to be shifted ahead since AdH goes first.

        # AdH Series value
        #DT_calculated = ags.adhseries[0].entry[ags.adhseries[0].size-2].time - ags.adhseries[0].entry[ags.adhseries[0].size-3].time
        #print"DT_calculated     =", DT_calculated, "s"
        #DT_calculated affects how the mass is distributed. If we want to dump all the mass from GSSHA into AdH's next time step
        #no matter how large it may be, we should use DT_calculated. For now, I'm skipping DT_calculated.
        seriesvalue = (2*DV/DT/ags.adhedgestringlen * ags.gsshahydrofact \
                    - ags.adhseries[0].entry[ags.adhseries[0].size-2].value[0])
        ags.adhseries[0].entry[ags.adhseries[0].size-2].value[0] = seriesvalue

        # Now set the last value same as the current value, but not the time!
        ags.adhseries[0].entry[ags.adhseries[0].size-1].time     = ags.adhseries[0].entry[ags.adhseries[0].size-2].time + TIME_TOL
        ags.adhseries[0].entry[ags.adhseries[0].size-1].value[0] = ags.adhseries[0].entry[ags.adhseries[0].size-2].value[0]

        for i in range(ags.adhseries[0].size-1):
            # Calculate slope
            ags.adhseries[0].entry[i].slope[0] = \
                    (ags.adhseries[0].entry[i+1].value[0] - ags.adhseries[0].entry[i].value[0]) / \
                    (ags.adhseries[0].entry[i+1].time - ags.adhseries[0].entry[i].time)#+1.0E-14)

            # Calculate 'area', i.e., volume/unit width that has flown in at this time step.
            ags.adhseries[0].entry[i].area[0]  = 0.5 * \
                    (ags.adhseries[0].entry[i+1].value[0] + ags.adhseries[0].entry[i].value[0]) * \
                    (ags.adhseries[0].entry[i+1].time - ags.adhseries[0].entry[i].time)

        #Store volume for the next time step.
        ags.gsshavoutprev   = ags.mvs[0].vout
        ags.gsshavoutprev_t = ags.mvs[0].timer

    else:

        for i in range (ags.adhseries[0].size-1):
            ags.adhseries[0].entry[i].time     = ags.adhseries[0].entry[i+1].time
            ags.adhseries[0].entry[i].value[0] = ags.adhseries[0].entry[i+1].value[0]
        ags.adhseries[0].entry[ags.adhseries[0].size-1].time     = ags.adhtfinal*10
        ags.adhseries[0].entry[ags.adhseries[0].size-1].value[0] = 0.0
        ags.adhseries[0].entry[ags.adhseries[0].size-2].value[0] = 0.0 # This one is important! Don't miss!
        for i in range (ags.adhseries[0].size-2):
            ags.adhseries[0].entry[i].slope[0] = ags.adhseries[0].entry[i+1].slope[0]
            ags.adhseries[0].entry[i].area[0]  = ags.adhseries[0].entry[i+1].area[0]
        ags.adhseries[0].entry[ags.adhseries[0].size-2].slope[0] = 0.0
        ags.adhseries[0].entry[ags.adhseries[0].size-2].area[0]  = 0.0


    if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) and DEBUG_LOCAL != 0 and ags.myid == 0:
        for i in range(ags.adhseries[0].size):
            print('After :(t,v)[',i,'] = (', ags.adhseries[0].entry[i].time,',',ags.adhseries[0].entry[i].value[0],')')
        print('Area contained    =', ags.adhseries[0].entry[ags.adhseries[0].size-3].area[0])



############################################################################################################
if __name__ == '__main__':
    pass

        #Discarded:
        #DT = 0.0
        #while (ags.sm[0].submodel[0].t_prev+DT < ags.mvs[0].timer*ags.gsshatimefact-ags.adhdt+TIME_TOL):
        #while (ags.sm[0].submodel[0].t_prev+DT < ags.mvs[0].timer*ags.gsshatimefact+ags.effectivegsshadt-TIME_TOL):
        #while (ags.mvs[0].niter*ags.gsshatimefact < mod[0].t_prev+ags.adhdt-TIME_TOL):
        #    DT += ags.adhdt
            #ags.adhseries[0].entry[ags.adhseries[0].size-2].time += DT #ags.adhdt # If 2-way adgda, then time series has to be shifted ahead since AdH goes first.

