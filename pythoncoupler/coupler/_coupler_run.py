#!/usr/bin/env python3
from __future__ import absolute_import, print_function
import ctypes as ct

############################################################################################################################################################
import adhpython.sclass.build_options   as adhopts
import adhpython.sclass.define_h        as adhdefine
import adhpython.sclass.fnctn_h         as adhfnctn
import adhpython.sclass.smodel_h        as adhsmodel

############################################################################################################################################################
import gsshapython.sclass.build_options as gsshaopts
import gsshapython.sclass.types_h       as gsshatypes
import gsshapython.sclass.define_h      as gsshadefine
import gsshapython.sclass.fnctn_h       as gsshafnctn

############################################################################################################################################################
from .adh_init_bc_func   import adh_init_bc_from_gssha_hydrograph
from .adh_set_bc_func    import adh_set_bc_from_gssha_hydrograph
from .gssha_init_bc_func import gssha_init_bc_from_adh_depths
from .gssha_set_bc_func  import gssha_set_bc_from_adh_depths

############################################################################################################################################################
DEBUG_LOCAL = 1
###########################################################################
# Note: coupler_run_gssha_driving_adh and coupler_run_adh_driving_gssha
# are very similar functions. The latter was created by copying the former,
# replacing the adh_init_bc, adh_set_bc functions to gssha_..., exchanging
# the gssha and adh running parts in the main while loop, and then only
# correcting the timing information in the nested while condition from
# -ags.adhdt+TIME_TOL to +ags.gsshadt-TIME_TOL, and
# +ags.adhdt-TIME_TOL to -ags.gsshadt+TIME_TOL.
# I wonder if this could have been combined into a single function?

############################################################################################################functag
def coupler_run_gssha_driving_adh(ags):

    from .adhgsshastruct     import TIME_TOL

    mod = ct.POINTER(adhsmodel.SMODEL)() # Alias, similar to SMODEL *mod; in C
    mod.contents = ags.sm[0].submodel[0] # similar to mod = &(sm[i].submodel[0]); in C

    dummyrun_time = ct.c_double(adhdefine.UNSET_FLT)

    adh_init_bc_from_gssha_hydrograph(ags)
    if ags.couplingtype == 'gdadg':
        gssha_init_bc_from_adh_depths(ags)

    # Set final times to zero.
    mod[0].t_final = 0.0
    ags.mvs[0].niter = 0
    # Run GSSHA only on 1 processsor: PE 0.
    if ags.myid == 0:
        ierr_code = gsshafnctn.main_gssha_run(ags.mvs)
        assert(ierr_code == 0)
        ags.mvs[0].go    = gsshatypes.TRUE
    else:
        # Assumes GSSHA cannot start at negative time!
        ags.mvs[0].go    = gsshatypes.FALSE
    if adhopts._MESSG == adhdefine.ON:
        if (adhopts._DEBUG ==adhdefine.ON or DEBUG_LOCAL != 0):
            print('PE[',ags.myid,'] Before messg: timer = ', ags.mvs[0].timer)
        ags.mvs[0].timer = adhfnctn.messg_dmax(ct.c_double(ags.mvs[0].timer), mod[0].grid[0].smpi[0].ADH_COMM)
        if (adhopts._DEBUG ==adhdefine.ON or DEBUG_LOCAL != 0):
            print('PE[',ags.myid,'] After messg : timer = ', ags.mvs[0].timer)

    while (mod[0].t_prev<ags.adhtfinal or ags.mvs[0].timer<ags.gsshatfinal):
        ######################################################
        if (ags.mvs[0].timer < ags.gsshatfinal):
            #while (ags.mvs[0].niter*ags.gsshatimefact < mod[0].t_prev+ags.adhdt-TIME_TOL):
            #    ags.mvs[0].niter             += int(ags.effectivegsshadt)/60
            #if (ags.adhrunflag==adhdefine.OFF): #If AdH is done first, let GSSHA finish off directly.
            #    ags.mvs[0].niter            = ags.gsshatfinal
            ## This one is the important one that determines end time:
            #ags.mvs[0].single_event_end = ags.mvs[0].b_lt_start + ags.mvs[0].niter/1440.0 #float(ags.mvs[0].niter)/1440.0

            # Decided while writing report. Driving model must take at least one time step forward.
            superdt = ags.effectivegsshadt
            #superdt = 0.0
            while (ags.mvs[0].timer*ags.gsshatimefact + superdt < mod[0].t_prev+ags.adhdt-TIME_TOL):
                superdt                    += ags.effectivegsshadt

            ags.mvs[0].niter               += int(max(1.0, (superdt+TIME_TOL)/60.0))
            # This one is the important one that determines end time:
            ags.mvs[0].single_event_end     = ags.mvs[0].b_lt_start + (ags.mvs[0].timer*ags.gsshatimefact + superdt)/86400.0 #Julian

            if (ags.adhrunflag==adhdefine.OFF): #If AdH is done first, let GSSHA finish off directly.
                ags.mvs[0].niter            = ags.gsshatfinal
                ags.mvs[0].single_event_end = ags.mvs[0].b_lt_start + ags.mvs[0].niter/1440.0 #gsshatfinal was original niter in mins

            if gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0 and ags.myid == 0:
                print("\n*******************************************\nRunning GSSHA:")
                print("dt             =", ags.mvs[0].dt)
                print("timer          =", ags.mvs[0].timer)
                print("niter          =", ags.mvs[0].niter)
                print("superdt        =", superdt)
                print("end time       =", ags.mvs[0].timer*ags.gsshatimefact + superdt)
            elif ags.myid==0:
                print("\n*******************************************\nRunning GSSHA:")

            # Run GSSHA only on 1 processsor: PE 0.
            if ags.myid == 0:
                ierr_code = gsshafnctn.main_gssha_run(ags.mvs)
                assert(ierr_code == 0)
                # Needed to force gssha to run for next time step:
                ags.mvs[0].go    = gsshatypes.TRUE
            else:
                # Note: We are keeping gssharunflag as ON, but mvs[0].go as FALSE!!
                # This matters in adh_set_bc functions!
                ags.mvs[0].go    = gsshatypes.FALSE
            if adhopts._MESSG == adhdefine.ON:
                if (adhopts._DEBUG ==adhdefine.ON or DEBUG_LOCAL != 0):
                    print('PE[',ags.myid,'] Before messg: timer = ', ags.mvs[0].timer)
                ags.mvs[0].timer = adhfnctn.messg_dmax(ct.c_double(ags.mvs[0].timer), mod[0].grid[0].smpi[0].ADH_COMM)
                if (adhopts._DEBUG ==adhdefine.ON or DEBUG_LOCAL != 0):
                    print('PE[',ags.myid,'] After messg : timer = ', ags.mvs[0].timer)

        else:
            ags.gssharunflag = gsshadefine.OFF
            ags.mvs[0].go    = gsshatypes.FALSE

        ######################################################
        # Set AdH Boundary conditions from GSSHA
        adh_set_bc_from_gssha_hydrograph(ags)

        ######################################################
        if (mod[0].t_prev < ags.adhtfinal):
            while (mod[0].t_final < ags.mvs[0].timer*ags.gsshatimefact-ags.adhdt+TIME_TOL):
                mod[0].t_final += ags.adhdt
            if (ags.gssharunflag == gsshadefine.OFF):
                mod[0].t_final = ags.adhtfinal

            if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0 and ags.myid == 0:
                print("\n*******************************************\nRunning AdH:")
                print("dt             =", mod[0].dt)
                print("t_prev         =", mod[0].t_prev)
                print("t_final        =", mod[0].t_final)
            elif ags.myid==0:
                print("\n*******************************************\nRunning AdH:")

            # Run AdH
            ierr_code = adhfnctn.adh_run_func_(ags.sm, ags.supiface,
                    ags.nsupmod, ags.nsupiface, ct.byref(dummyrun_time))
            assert(ierr_code == 0)
            # Have to reset AdH dt. Should use tc_init ideally, I think.
            mod[0].dt = ags.adhdt

        else:
            ags.adhrunflag=adhdefine.OFF

        ######################################################
        ## Set GSSHA Boundary conditions from AdH
        if ags.couplingtype == 'gdadg':
            gssha_set_bc_from_adh_depths(ags)

############################################################################################################functag
def coupler_run_adh_driving_gssha(ags):

    from .adhgsshastruct     import TIME_TOL

    mod = ct.POINTER(adhsmodel.SMODEL)() # Alias, similar to SMODEL *mod; in C
    mod.contents = ags.sm[0].submodel[0] # similar to mod = &(sm[i].submodel[0]); in C

    dummyrun_time = ct.c_double(adhdefine.UNSET_FLT)

    gssha_init_bc_from_adh_depths(ags)
    if ags.couplingtype == 'adgda':
       adh_init_bc_from_gssha_hydrograph(ags)

    # Set final times to zero.
    mod[0].t_final = mod[0].t_init
    ags.mvs[0].niter = 0
    # Run GSSHA only on 1 processsor: PE 0.
    if ags.myid == 0:
        ierr_code = gsshafnctn.main_gssha_run(ags.mvs)
        assert(ierr_code == 0)
        ags.mvs[0].go    = gsshatypes.TRUE
    else:
        # Assumes GSSHA cannot start at negative time!
        ags.mvs[0].go    = gsshatypes.FALSE
    if adhopts._MESSG == adhdefine.ON:
        if (adhopts._DEBUG ==adhdefine.ON or DEBUG_LOCAL != 0):
            print('PE[',ags.myid,'] Before messg: timer = ', ags.mvs[0].timer)
        ags.mvs[0].timer = adhfnctn.messg_dmax(ct.c_double(ags.mvs[0].timer), mod[0].grid[0].smpi[0].ADH_COMM)
        if (adhopts._DEBUG ==adhdefine.ON or DEBUG_LOCAL != 0):
            print('PE[',ags.myid,'] After messg : timer = ', ags.mvs[0].timer)

    while (mod[0].t_prev<ags.adhtfinal or ags.mvs[0].timer<ags.gsshatfinal):
        ######################################################
        if (mod[0].t_prev < ags.adhtfinal):
            # Decided while writing report. Driving model must take at least one time step forward.
            superdt = ags.adhdt
            while (mod[0].t_final + superdt < ags.mvs[0].timer*ags.gsshatimefact+ags.effectivegsshadt-TIME_TOL):
                superdt += ags.adhdt
            mod[0].t_final += superdt
            #while (mod[0].t_final < ags.mvs[0].timer*ags.gsshatimefact+ags.effectivegsshadt-TIME_TOL):
            #    mod[0].t_final += ags.adhdt
            if (ags.gssharunflag == gsshadefine.OFF):
                mod[0].t_final = ags.adhtfinal

            if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0 and ags.myid == 0 :
                print("\n*******************************************\nRunning AdH:")
                print("dt             =", mod[0].dt)
                print("t_prev         =", mod[0].t_prev)
                print("t_final        =", mod[0].t_final)
            elif ags.myid==0:
                print("\n*******************************************\nRunning AdH:")

            # Run AdH
            ierr_code = adhfnctn.adh_run_func_(ags.sm, ags.supiface,
                    ags.nsupmod, ags.nsupiface, ct.byref(dummyrun_time))
            assert(ierr_code == 0)
            # Have to reset AdH dt. Should use tc_init ideally, I think.
            mod[0].dt = ags.adhdt
        else:
            ags.adhrunflag=adhdefine.OFF

        ######################################################
        ## Set GSSHA Boundary conditions from AdH
        gssha_set_bc_from_adh_depths(ags)

        ######################################################
        if (ags.mvs[0].timer < ags.gsshatfinal):
            #while (ags.mvs[0].niter*ags.gsshatimefact < mod[0].t_prev-ags.effectivegsshadt+TIME_TOL):
            #    ags.mvs[0].niter += int(ags.effectivegsshadt)/60
            #if (ags.adhrunflag==adhdefine.OFF): #If AdH is done first, let GSSHA finish off directly.
            #    ags.mvs[0].niter  = ags.gsshatfinal
            ## This is the important one that determines end time:
            #ags.mvs[0].single_event_end = ags.mvs[0].b_lt_start + ags.mvs[0].niter/1440.0 #float(ags.mvs[0].niter)/1440.0
            superdt = 0.0
            while (ags.mvs[0].timer*ags.gsshatimefact + superdt < mod[0].t_prev-ags.effectivegsshadt+TIME_TOL):
                superdt                    += ags.effectivegsshadt

            ags.mvs[0].niter               += int(max(1.0, (superdt+TIME_TOL)/60.0))
            # This one is the important one that determines end time:
            ags.mvs[0].single_event_end     = ags.mvs[0].b_lt_start + (ags.mvs[0].timer*ags.gsshatimefact + superdt)/86400.0 #Julian

            if (ags.adhrunflag==adhdefine.OFF): #If AdH is done first, let GSSHA finish off directly.
                ags.mvs[0].niter            = ags.gsshatfinal
                ags.mvs[0].single_event_end = ags.mvs[0].b_lt_start + ags.mvs[0].niter/1440.0 #gsshatfinal was original niter in mins

            if gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0 and ags.myid == 0:
                print("\n*******************************************\nRunning GSSHA:")
                print("dt             =", ags.mvs[0].dt)
                print("timer          =", ags.mvs[0].timer)
                print("niter          =", ags.mvs[0].niter)
                print("superdt        =", superdt)
                print("end time       =", ags.mvs[0].timer*ags.gsshatimefact + superdt)
            elif ags.myid==0:
                print("\n*******************************************\nRunning GSSHA:")

            # Run GSSHA only on 1 processsor: PE 0.
            if ags.myid == 0:
                ierr_code = gsshafnctn.main_gssha_run(ags.mvs)
                assert(ierr_code == 0)
                # Needed to force gssha to run for next time step:
                ags.mvs[0].go    = gsshatypes.TRUE
            else:
                # Note: We are keeping gssharunflag as ON, but mvs[0].go as FALSE!!
                # This matters in adh_set_bc functions!
                ags.mvs[0].go    = gsshatypes.FALSE
            if adhopts._MESSG == adhdefine.ON:
                if (adhopts._DEBUG ==adhdefine.ON or DEBUG_LOCAL != 0):
                    print('PE[',ags.myid,'] Before messg: timer = ', ags.mvs[0].timer)
                ags.mvs[0].timer = adhfnctn.messg_dmax(ct.c_double(ags.mvs[0].timer), mod[0].grid[0].smpi[0].ADH_COMM)
                if (adhopts._DEBUG ==adhdefine.ON or DEBUG_LOCAL != 0):
                    print('PE[',ags.myid,'] After messg : timer = ', ags.mvs[0].timer)

        else:
            ags.gssharunflag = gsshadefine.OFF
            ags.mvs[0].go    = gsshatypes.FALSE

        ######################################################
        # Set AdH Boundary conditions from GSSHA
        if ags.couplingtype == 'adgda':
            # Also note: mvs[0].timer for PE!=0 is incremented in adh_set_bc functions!
            adh_set_bc_from_gssha_hydrograph(ags)

############################################################################################################functag
def adhgssha_coupler_run(self):

    if self.couplingtype == 'gda':
        run_string = 'Running GSSHA driving AdH, One-way coupling'
        run_func = coupler_run_gssha_driving_adh

    elif self.couplingtype == 'adg':
        run_string = 'Running AdH driving GSSHA, One-way coupling'
        run_func = coupler_run_adh_driving_gssha

    elif self.couplingtype == 'gdadg':
        run_string = 'Running GSSHA driving AdH driving GSSHA, Two-way coupling'
        run_func = coupler_run_gssha_driving_adh

    elif self.couplingtype == 'adgda':
        run_string = 'Running AdH driving GSSHA driving AdH, Two-way coupling'
        run_func = coupler_run_adh_driving_gssha

    else:
        print('Unkown coupling type supplied by user:', self.couplingtype, '\nExiting.')
        return

    print("\n\n*****************************************************************")
    print(    "*****************************************************************")
    print(    run_string)
    print(    "*****************************************************************")
    print(    "*****************************************************************")

    run_func(self)

    print("\n\n*****************************************************************")
    print(    "*****************************************************************")
    print(    "Finished", run_string)
    print(    "*****************************************************************")
    print(    "*****************************************************************")


############################################################################################################functag
if __name__ == '__main__':
    pass

