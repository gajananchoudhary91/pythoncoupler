#!/usr/bin/env python3
from __future__ import absolute_import, print_function
import ctypes as ct

############################################################################################################################################################
import adhpython.sclass.build_options   as adhopts
import adhpython.sclass.define_h        as adhdefine
import adhpython.sclass.fnctn_h         as adhfnctn
import adhpython.sclass.sgrid_h         as adhsgrid

############################################################################################################################################################

############################################################################################################################################################
DEBUG_LOCAL = 1
############################################################################################################
def adh_init_bc_from_gssha_hydrograph(ags): # ags is of type adhgsshatruct.

    from pythoncoupler.coupler.adhgsshastruct import SERIESLENGTH, TIME_TOL

    ######################################################
    #SET UP AdH BC series and edgestring.
    ######################################################
    # Find edge string to couple.
    # Hardcoding for now!!!
    seriesid = ags.sm[0].submodel[0].str_values[ags.adhedgestringid].ol_flow.isigma
    print("\nAdH Edge string ID", ags.adhedgestringid+1, "corresponds to BC series", seriesid+1)

    # Find series to modify during coupling.
    ags.adhseries = ags.sm[0].submodel[0].series_head
    while (ags.adhseries):
        #if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0 and ags.myid==0:
        #    print("Series ID:", ags.adhseries[0].id)
        if ags.adhseries[0].id == seriesid:
            break
        else:
            ags.adhseries = ags.adhseries[0].next
    if (ags.adhseries):
        if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0 and ags.myid==0:
            print("Series (",ags.adhseries[0].id+1,") will be modified while coupling GSSHA and AdH.")
        assert (ags.adhseries[0].size >= SERIESLENGTH)
        # entry[series_size-2].time must be set to mod->t_init.

        if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0 and ags.myid==0:
            print()
            for i in range(ags.adhseries[0].size):
                print("Before: (t,v) = (",ags.adhseries[0].entry[i].time,",",ags.adhseries[0].entry[i].value[0],")")

        if ags.adhtstart > 0.0: # GSSHA starting time is before AdH starting time; GSSHA always starts at 0.0, hopefully!
            superdt = ags.adhdt
        else:
            superdt = ags.sm[0].submodel[0].t_init
            while (superdt < ags.effectivegsshadt - TIME_TOL):
                superdt += ags.adhdt #max(ags.effectivegsshadt, ags.adhdt)
            superdt -= ags.sm[0].submodel[0].t_init

        for i in range(ags.adhseries[0].size):
            ags.adhseries[0].entry[i].value[0] = 0.0
            ags.adhseries[0].entry[i].time = ags.sm[0].submodel[0].t_init - (ags.adhseries[0].size-2-i)*superdt
            if ags.couplingtype == 'adgda':
                ags.adhseries[0].entry[i].time += superdt # If 2-way adgda, then time series has to be shifted ahead since AdH goes first.

        # Play around with this if you are having problem with AdH BC Series Starting time.
        if ags.sm[0].submodel[0].t_init > 0:  # If AdH starting time is later than GSSHA. GSSHA always starts at 0.
            for i in range(ags.adhseries[0].size-2):
                ags.adhseries[0].entry[i].time -= ags.sm[0].submodel[0].t_init
            if ags.couplingtype != 'adgda':
                ags.adhseries[0].entry[ags.adhseries[0].size-2].time -= ags.sm[0].submodel[0].t_init

        for i in range(ags.adhseries[0].size):
            if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0 and ags.myid==0:
                print("After : (t,v) = (",ags.adhseries[0].entry[i].time,",",ags.adhseries[0].entry[i].value[0],")")
    else:
        print("Error! Could not find series with ID", seriesid)
        exit()

    grid=ct.POINTER(adhsgrid.SGRID)()
    grid.contents = ags.sm[0].submodel[0].grid[0]
    for ie in range(grid[0].nelems1d):
        if ((grid[0].elem1d[ie].string == ags.adhedgestringid)
        and (grid[0].node[grid[0].elem1d[ie].nodes[0]].resident_pe == grid[0].smpi[0].myid)
        and (grid[0].node[grid[0].elem1d[ie].nodes[1]].resident_pe == grid[0].smpi[0].myid)):
            ags.adhedgestringlen += grid[0].elem1d[ie].djac
    if adhopts._MESSG==adhdefine.ON:
        ags.adhedgestringlen = adhfnctn.messg_dsum(ags.adhedgestringlen, grid[0].smpi[0].ADH_COMM)
    if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0:
        print("Edge string(",ags.adhedgestringid,"): Length = ", ags.adhedgestringlen)

############################################################################################################
if __name__ == '__main__':
    pass

