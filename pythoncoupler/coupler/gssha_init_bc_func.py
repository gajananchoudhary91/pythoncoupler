#!/usr/bin/env python3
from __future__ import absolute_import, print_function
import ctypes as ct

############################################################################################################################################################
import gsshapython.sclass.build_options   as gsshaopts
import gsshapython.sclass.define_h        as gsshadefine
import gsshapython.sclass.fnctn_h         as gsshafnctn
from gsshapython.sclass.timeseriesdefs_h import ts_struct

############################################################################################################################################################
import adhpython.sclass.build_options     as adhopts
import adhpython.sclass.define_h          as adhdefine
import adhpython.sclass.fnctn_h           as adhfnctn
import adhpython.sclass.smodel_h          as adhsmodel
import adhpython.sclass.sgrid_h           as adhsgrid

############################################################################################################################################################

############################################################################################################################################################
DEBUG_LOCAL = 1
############################################################################################################
def gssha_init_bc_from_adh_depths_discontinuous(ags): # ags is of type adhgsshatruct.
    # It so happens that this one is same as the init_bc function counterpart!
    assert(ags.mvs[0].yes_head_bound == 1)
    assert(ags.mvs[0].bound_ts != 1)
    return gssha_set_bc_from_adh_depths_discontinuous(ags)

############################################################################################################
def gssha_init_bc_from_adh_depths(ags): # ags is of type adhgsshatruct.
    from .adhgsshastruct import SERIESLENGTH
    assert(ags.mvs[0].yes_head_bound == 1)
    assert(ags.mvs[0].bound_ts == 1)
    assert(ags.mvs[0].bound_ts_ptr)
    assert(ags.mvs[0].bound_ts_ptr[0].num_vals >= SERIESLENGTH)

    ######################################################
    #SET UP gssha BC from AdH.
    ######################################################
    # Find the value of maximum depth first.
    mod=ct.POINTER(adhsmodel.SMODEL)()
    mod.contents = ags.sm[0].submodel[0]
    grid=ct.POINTER(adhsgrid.SGRID)()
    grid.contents = ags.sm[0].submodel[0].grid[0]
    my_max_h = -1.0e+200
    my_min_h =  1.0e+200
    count=0.0
    h=0.0
    for ie in range(grid[0].nelems1d):
        if (grid[0].elem1d[ie].string == ags.adhedgestringid):
            n1 = grid[0].elem1d[ie].nodes[0]
            n2 = grid[0].elem1d[ie].nodes[1]
            h1 = mod[0].sw[0].d2[0].head[n1]
            h2 = mod[0].sw[0].d2[0].head[n2]
            if (grid[0].node[n1].resident_pe == grid[0].smpi[0].myid):
                h  += h1*grid[0].elem1d[ie].djac  # Average depth calculation
                my_max_h = max(my_max_h, h1)
                my_min_h = min(my_min_h, h1)
                count    += grid[0].elem1d[ie].djac
            if (grid[0].node[n2].resident_pe == grid[0].smpi[0].myid):
                h  += h2*grid[0].elem1d[ie].djac  # Average depth calculation
                my_max_h = max(my_max_h, h2)
                my_min_h = min(my_min_h, h2)
                count    += grid[0].elem1d[ie].djac

    # Note: Hoping whichever depth is closes to GSSHA current depth works better in damping oscillations than dmax alone!
    if (adhopts._MESSG==adhdefine.ON):
        h     = adhfnctn.messg_dsum(ct.c_double(h)       , grid[0].smpi[0].ADH_COMM)
        max_h = adhfnctn.messg_dmax(ct.c_double(my_max_h), grid[0].smpi[0].ADH_COMM)
        min_h = adhfnctn.messg_dmin(ct.c_double(my_min_h), grid[0].smpi[0].ADH_COMM)
        count = adhfnctn.messg_dsum(ct.c_double(count)   , grid[0].smpi[0].ADH_COMM)
    else:
        max_h = my_max_h
        min_h = my_min_h
    h=h/count
    ags.adh_hprev = h # Going to be taking the average.
    ags.adh_hprev_len = count # Going to be taking the average.
    print("PE[",ags.myid,"] Edge string(",ags.adhedgestringid,"): Starting Average h = ", ags.adh_hprev, "count = ", ags.adh_hprev_len)


    ######################################################
    ts = ags.mvs[0].bound_ts_ptr[0]
    if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) and DEBUG_LOCAL != 0 and ags.myid == 0:
        print('\nSetting up Boundary time series for GSSHA.')
        for i in range(ts.num_vals):
            print('Before:(t,v)[',i,'] = (', ts.jul_time[i],',', ts.val[i],')')

    superdt = 0.0
    while (superdt < ags.adhtstart + ags.adhdt):
        # Should be the commented one ideally, but GSSHA starts with a minimum 1 minute run, causing issues. Had to replace with max(...)
        superdt += ags.effectivegsshadt
        #superdt += max(60.0, ags.effectivegsshadt)
    superdt = superdt/86400.0

    for i in range(ts.num_vals):
        ts.jul_time[i] = ags.mvs[0].btime - (ts.num_vals-2-i) * superdt # max(ags.effectivegsshadt, ags.adhdt)/86400.0
        ts.val[i]      = ags.mvs[0].boundary_depth
        if ags.couplingtype == 'gdadg':
            ts.jul_time[i] += superdt #max(ags.effectivegsshadt, ags.adhdt)/86400.0
    ts.jul_time[ts.num_vals-1] = ts.jul_time[ts.num_vals-2] + ags.effectivegsshadt/86400.0 # max(ags.effectivegsshadt, ags.adhdt)/86400.0

    if (adhopts._DEBUG ==adhdefine.ON or gsshaopts._DEBUG == gsshadefine.ON) and DEBUG_LOCAL != 0 and ags.myid == 0:
        for i in range(ts.num_vals):
            print('After :(t,v)[',i,'] = (', ts.jul_time[i],',', ts.val[i],')')
    #exit()

############################################################################################################
if __name__ == '__main__':
    pass

