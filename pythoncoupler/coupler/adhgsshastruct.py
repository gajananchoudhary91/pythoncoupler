#!/usr/bin/env python3
from __future__ import absolute_import, print_function
import ctypes as ct

############################################################################################################################################################
import adhpython.sclass.define_h        as adhdefine
import adhpython.sclass.fnctn_h         as adhfnctn
import adhpython.sclass.ssuperModel_h   as adhsupmod
import adhpython.sclass.ssuperinterface_h   as adhsupiface
import adhpython.sclass.sseries_h       as adhsseries
from   adhpython.sclass.build_types import MPI_Comm

############################################################################################################################################################
import gsshapython.sclass.define_h      as gsshadefine
import gsshapython.sclass.fnctn_h       as gsshafnctn
import gsshapython.sclass.main_struct_h as gsshamain

############################################################################################################################################################
TIME_TOL = 1.0E-3
SERIESLENGTH = 4 #This is the MINIMUM number of lines required in an AdH series to be coupled. Compulsory.
GSSHA_TIME_FACTOR = 60.0 #Minutes to second conversion
GSSHA_CUFTPERSEC_TO_CUMPERSEC = 0.028316846592 # cu.ft/s to cu.m/s factor
DEBUG_LOCAL = 1

class adhgsshastruct(): #Note: This is not a ctypes Structure!!!!
    def __init__(self):
        # Main AdH and GSSHA structures
        self.sm = ct.POINTER(adhsupmod.SSUPER_MODEL)()
        self.nsupmod = ct.c_int(0)
        self.supiface = ct.POINTER(adhsupiface.SSUPER_INTERFACE)()
        self.nsupiface = ct.c_int(0)
        self.mvs = gsshafnctn.get_lib_var(gsshafnctn.gsshalib,'python_main_var_struct_ptr', ct.POINTER(gsshamain.main_var_struct))
        # Pythoncoupler data
        self.couplingtype = ''
        self.npes = 0
        self.myid = 0

        # AdH data
        self.adhrunflag=adhdefine.ON
        self.adhdt=0.0
        self.adhtstart=0.0
        self.adhtprev=0.0
        self.adhtfinal=0.0
        if MPI_Comm is not None:
            self.adh_comm_world=MPI_Comm()
        self.adhseries= ct.POINTER(adhsseries.SSERIES)()
        self.adhedgestringid=adhdefine.UNSET_INT
        self.adhedgestringlen=0.0
        self.adh_hprev=0.0   # Avg depth
        self.adh_hprev_len=0.0   # count

        # GSSHA data
        self.gssharunflag=gsshadefine.ON
        self.gsshatstartjul=0.0
        self.gsshadt=0.0
        self.effectivegsshadt=0.0
        self.gsshatprev=0.0
        self.gsshatfinal=0.0
        self.gsshasingle_event_end=0.0
        self.gsshavoutprev=0.0
        self.gsshavoutprev_t=0.0
        self.gsshatimefact=GSSHA_TIME_FACTOR ## Minutes to seconds conversion, since niter is in mins.
        self.gsshahydrofact=1.0 # GSSHA_CUFTPERSEC_TO_CUMPERSEC may not be required after all since GSSHA internally seems to use cu.m/s.

    from ._coupler_initialize import adhgssha_coupler_initialize as coupler_initialize
    from ._coupler_run import adhgssha_coupler_run as coupler_run
    from ._coupler_finalize import adhgssha_coupler_finalize as coupler_finalize


############################################################################################################################################################
if __name__=='__main__':
    argv = ct.POINTER(ct.c_char_p)()
    argc = ct.c_int()
    ct.pythonapi.Py_GetArgcArgv(ct.byref(argc), ct.byref(argv))

    if DEBUG_LOCAL == 1:
        print('Number of arguments passed to python: {0} \nArgs:'.format(
            argc.value), end = '')
        [print(argv[i], end = ' ') for i in range(argc.value)]
        print()

    if argc.value>=6:
        print("AdH project   :", argv[argc.value-1])
        print("GSSHA project :", argv[argc.value-2])
        ags = adhgsshastruct()
        ags.coupler_initialize(argc, argv)
        ags.coupler_run()
        ags.coupler_finalize()
    else:
        print("\nPath to AdH and GSSHA input files not specified."
              "\nExiting without testing.")

