#!/usr/bin/env python3
from __future__ import absolute_import, print_function
from ctypes import byref

############################################################################################################################################################
import adhpython.sclass.build_options as adhopts
import adhpython.sclass.define_h      as adhdefine
import adhpython.sclass.fnctn_h       as adhfnctn

############################################################################################################################################################
import gsshapython.sclass.build_options as gsshaopts
import gsshapython.sclass.define_h      as gsshadefine
import gsshapython.sclass.fnctn_h       as gsshafnctn

############################################################################################################################################################
DEBUG_LOCAL = 1
############################################################################################################
def adhgssha_coupler_finalize(self):

    if adhopts._DEBUG==adhdefine.ON and DEBUG_LOCAL!=0 and self.myid==0:
        print('\n\nFinalizing AdH\n')
    ierr_code = adhfnctn.adh_finalize_func_(byref(self.sm),
            byref(self.supiface), self.nsupmod, self.nsupiface)
    if self.myid==0:
        print("************************* AdH Finalized *************************")
        print("*****************************************************************")

    if gsshaopts._DEBUG==gsshadefine.ON and DEBUG_LOCAL!=0 and self.myid==0:
        print("Finalizing GSSHA")
    ierr_code = gsshafnctn.main_gssha_finalize(self.mvs)
    if self.myid==0:
        print("************************ GSSHA Finalized ************************")
        print("*****************************************************************")

    if adhopts._MESSG==adhdefine.ON:
        if adhopts._DEBUG==adhdefine.ON and DEBUG_LOCAL!=0 and self.myid==0:
            print('Finalizing MPI')
        ierr_code = adhfnctn.adhpython_mpi_finalize()
        if self.myid==0:
            print("************************* MPI Finalized *************************")
            print("*****************************************************************")


############################################################################################################

if __name__ == '__main__':
    pass
