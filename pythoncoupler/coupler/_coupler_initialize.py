#!/usr/bin/env python3
from __future__ import absolute_import, print_function
import ctypes as ct

DEBUG_LOCAL = 0

############################################################################################################################################################
def adhgssha_coupler_initialize(self, couplingtype, argc, argv):
    # argv[argc-1] must be AdH model
    # argv[argc-2] must by GSSHA model
    # argv[argc-3] must be coupling type: 'gda', 'adg', 'gdadg', 'adgda'
    # argv[argc-4] must be edge string ID of the AdH model that we are coupling to

    ######################################################
    import adhpython.sclass.build_options as adhopts
    import adhpython.sclass.define_h      as adhdefine
    import adhpython.sclass.fnctn_h       as adhfnctn
    import adhpython.sclass.sgrid_h       as adhsgrid
    import adhpython.sclass.ssuperModel_h as adhsupmod
    import adhpython.sclass.ssuperinterface_h as adhsupiface

    ######################################################
    import gsshapython.sclass.build_options as gsshaopts
    import gsshapython.sclass.define_h      as gsshadefine
    import gsshapython.sclass.fnctn_h       as gsshafnctn

    byref = ct.byref

    ######################################################
    #SET UP ADH.
    ######################################################
    print('Beginning calls to AdH shared library...')
    if adhdefine._MESSG == adhdefine.ON:
        ierr_code = adhfnctn.adhpython_mpi_initialize(byref(self.adh_comm_world))
        if adhopts._DEBUG == adhdefine.ON:
            print('Python: adh_comm_world pointer value: {0}'.format(
                hex(self.adh_comm_world.value)))
        print("************************ MPI Initialized ************************")
        print("*****************************************************************")
    else:
        ierr_code = adhfnctn.adhpython_mpi_initialize()

    ######################################################
    print('Initializing AdH')
    if adhdefine._MESSG == adhdefine.ON:
        ierr_code = adhfnctn.adh_init_func_(byref(self.sm), byref(self.supiface),
                byref(self.nsupmod), byref(self.nsupiface),
                byref(argc), byref(argv), byref(self.adh_comm_world))
    else:
        ierr_code = adhfnctn.adh_init_func_(byref(self.sm), byref(self.supiface),
                byref(self.nsupmod), byref(self.nsupiface),
                byref(argc), byref(argv))
    assert(self.nsupmod.value == 1)
    assert(ierr_code == 0)
    self.npes = self.sm[0].submodel[0].grid[0].smpi[0].npes
    self.myid = self.sm[0].submodel[0].grid[0].smpi[0].myid
    if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0:
        print(self.sm[0])
        print("MPI Info: npes =", self.npes, ", myid =", self.myid)

    print("************************ AdH Initialized ************************")
    print("*****************************************************************")

    ######################################################
    #SET UP GSSHA.
    ######################################################
    if gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0:
        print("\nInitializing GSSHA\n")
    prj_name = argv[argc.value-2]
    ierr_code = gsshafnctn.main_gssha_initialize(ct.byref(self.mvs), prj_name, None, prj_name)
    assert(ierr_code == 0)
    print("*********************** GSSHA Initialized ***********************")
    print("*****************************************************************")

    ######################################################
    #SET UP COUPLED STRUCT.
    ######################################################
    self.couplingtype=couplingtype #argv[argc.value-3]
    self.adhrunflag=adhdefine.ON
    self.adhtstart=self.sm[0].submodel[0].t_init
    self.adhdt=self.sm[0].submodel[0].dt
    self.adhtprev=self.sm[0].submodel[0].t_prev
    self.adhtfinal=self.sm[0].submodel[0].t_final
    self.adhedgestringid=int(argv[argc.value-4])-1
    self.gssharunflag=gsshadefine.ON
    self.gsshatstartjul=self.mvs[0].btime # in Julian date
    self.gsshadt=self.mvs[0].dt # in seconds
    #self.effectivegsshadt=max(60.0, self.mvs[0].dt) # in seconds. This is in case we decide to use niter in mins as ending time
    self.effectivegsshadt=self.mvs[0].dt # in seconds. This is in case we decide to use single_event_end time as ending time
    self.gsshatprev=self.mvs[0].timer # in minutes
    self.gsshatfinal=self.mvs[0].niter # in minutes
    self.gsshasingle_event_end=self.mvs[0].single_event_end # in minutes

############################################################################################################
if __name__ == '__main__':
    pass

