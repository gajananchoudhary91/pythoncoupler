#!/usr/bin/env python3
#------------------------------------------------------------------------------#
# pythoncoupler - Software for coupling hydrodynamic and hydrologic software
#------------------------------------------------------------------------------#
"""
Context module for adding system path for integration tests.
"""
from __future__ import absolute_import, print_function
import os
import sys

pythoncoupler_root=os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__)
            ))))
        )
sys.path.insert(0, os.path.abspath(pythoncoupler_root))

fixture_dir=os.path.join(os.path.dirname(os.path.abspath(__file__)),'fixtures')

__all__ = ['fixture_dir',]

