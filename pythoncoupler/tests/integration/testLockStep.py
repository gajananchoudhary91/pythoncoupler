# some_file.py
from __future__ import absolute_import, print_function, division
import os
import unittest
import ctypes as ct
from ctypes import byref

if __name__ == '__main__':
    # Executing test directly over command line
    from integrationcontext import *
else:
    # Executing test as module
    from .integrationcontext import *

#------------------------------------------------------------------------------#
import adhpython.sclass.build_options as adhopts
import adhpython.sclass.define_h as adhdefine
import adhpython.sclass.fnctn_h as adhfnctn
from adhpython.sclass.build_types import MPI_Comm
from adhpython.sclass.ssolve_h import Solver_Info
from adhpython.sclass.ssuperModel_h import SSUPER_MODEL
from adhpython.sclass.ssuperinterface_h import SSUPER_INTERFACE
from adhpython.sclass.smodel_h import SMODEL
from adhpython.sclass.fnctn_h import adhlib, print_var, \
        adhpython_mpi_initialize, adhpython_mpi_finalize, \
        adh_init_func_, adh_run_func_, adh_finalize_func_

#------------------------------------------------------------------------------#
import gsshapython.sclass.build_options as gsshaopts
import gsshapython.sclass.define_h as gsshadefine
import gsshapython.sclass.types_h as gsshatypes
import gsshapython.sclass.fnctn_h as gsshafnctn
from gsshapython.sclass.main_struct_h import main_var_struct
from gsshapython.sclass.fnctn_h import gsshalib, \
        main_gssha_initialize, main_gssha_run, \
        main_gssha_finalize

#------------------------------------------------------------------------------#
DEBUG_LOCAL = 1
current_dir = os.getcwd()
testcase_dir = os.path.join(fixture_dir, "small_stream_coupling")
try: # Python 3x
    argvadh   = (ct.c_char_p*2)(b"python", b"small_2d")
    argvgssha = (ct.c_char_p*2)(b"python", b"Stream.prj")
except: # Python 2x
    argvadh   = (ct.c_char_p*2)("python", "small_2d")
    argvgssha = (ct.c_char_p*2)("python", "Stream.prj")

#------------------------------------------------------------------------------#
class TestLockStep(unittest.TestCase):
        #--------------------------------------------------------#
    def setUp(self):

        #--------------------------------------------------------#
        #SET UP ADH.
        #--------------------------------------------------------#
        os.chdir(testcase_dir)
        self.argv = ct.cast(argvadh, ct.POINTER(ct.c_char_p))
        self.argc = ct.c_int(2)

        if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0:
            print('Number of arguments being set in test case: {0}\nArgs:'.format(
                self.argc.value), end = '')
            [print(self.argv[i], end = ' ') for i in range(self.argc.value)]
            print()

        self.run_time  = ct.c_double(-3.0)

        #----------------------------------------------------#
        self.supmod = ct.POINTER(SSUPER_MODEL)()
        self.nsupmod = ct.c_int(0)

        self.supifc = ct.POINTER(SSUPER_INTERFACE)()
        self.nsupifc = ct.c_int(0)

        #----------------------------------------------------#
        if adhopts._MESSG == adhdefine.ON:
            self.adh_comm_world = MPI_Comm()
            ierr_code = adhpython_mpi_initialize(ct.byref(self.adh_comm_world))
            if (adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL!=0):
                print('Python: adh_comm_world pointer value: {0}'.format(
                    hex(self.adh_comm_world.value)))
            print("************************ MPI Initialized ************************\n")
            print("*****************************************************************\n")
        else:
            ierr_code = adhpython_mpi_initialize()

        #--------------------------------------------------------#
        print("\n\nInitializing AdH\n")
        if adhopts._MESSG == adhdefine.ON:
            ierr_code = adh_init_func_(byref(self.supmod), byref(self.supifc),
                    byref(self.nsupmod), byref(self.nsupifc),
                    byref(self.argc), byref(self.argv), byref(self.adh_comm_world))
        else:
            ierr_code = adh_init_func_(byref(self.supmod), byref(self.supifc),
                    byref(self.nsupmod), byref(self.nsupifc),
                    byref(self.argc), byref(self.argv))
        self.assertEqual(ierr_code, 0) #Since supmod is not NULL now
        self.assertIsNotNone(self.supmod)
        self.check_values_adh(self.supmod,self.nsupmod)

        print("************************ AdH Initialized ************************\n")
        print("*****************************************************************\n")
        #--------------------------------------------------------#
        #SET UP GSSHA.
        #--------------------------------------------------------#
        if (gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL!=0):
            print("Getting model_var_struct pointer from C++ to Python")
        self.mvs = gsshafnctn.get_lib_var(gsshalib, 'python_main_var_struct_ptr', ct.POINTER(main_var_struct))
        self.assertFalse(self.mvs)

        #--------------------------------------------------------#
        if gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0:
            print("Initializing GSSHA")
        prj_name = argvgssha[1]
        ierr_code = main_gssha_initialize(ct.byref(self.mvs), prj_name, None, prj_name)
        self.assertIsNotNone(self.mvs.contents)
        self.check_values_gssha(self.mvs)
        print("*********************** GSSHA Initialized ***********************\n")
        print("*****************************************************************\n")


#------------------------------------------------------------------------------#
    def tearDown(self):

        #--------------------------------------------------------#
        if gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0:
            print("Finalizing GSSHA")
        ierr_code = main_gssha_finalize(self.mvs)
        print("************************ GSSHA Finalized ************************\n")
        print("*****************************************************************\n")

        print('\n\nFinalizing AdH\n')
        ierr_code = adh_finalize_func_(byref(self.supmod), byref(self.supifc),
            self.nsupmod, self.nsupifc)
        print("************************* AdH Finalized *************************\n")
        print("*****************************************************************\n")

        if adhopts._MESSG == adhdefine.ON:
            if (adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL!=0):
                print('Finalizing MPI')
            ierr_code = adhpython_mpi_finalize()
            print("************************* MPI Finalized *************************\n")
            print("*****************************************************************\n")

        # Go back to original starting directory.
        os.chdir(testcase_dir)

#------------------------------------------------------------------------------#
    def check_values_adh(self, sm, nsupmod):
        # Analyze values and addresses!
        if (adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0):
            print('\nVariable, Value')
            print('Python object:', sm)
            print('P: id(sm),                   '+ print_var(sm, 1111))
            print('C: &sm,                      '+ print_var(sm, 111 ))
            mod = ct.POINTER(SMODEL)() # Alias, similar to SMODEL *mod; in C
            info = ct.POINTER(Solver_Info)()
            for i in range(nsupmod.value):
                mod.contents = sm[i].submodel[0] # similar to mod = &(sm[i].submodel[0]); in C
                print('C:   &sm['+str(i)+'],                 '+ print_var(sm[i]               , 111 ))
                print('C:   &sm['+str(i)+'].mod,             '+ print_var(sm[i].submodel      , 111 ))
                print('-:     nsubmodels,           '         + print_var(sm[i].nsubmodels    , 1   ))
                print('C:     submodel,             '         + print_var(mod                 , 333 ))
                for j in range(sm[i].nsubmodels):
                    print('C:     &mod['+str(j)+'],              '+ print_var(mod[j]              , 111 ))
                for j in range(sm[i].nsubmodels):
                    info.contents = mod[j].solver_info
                    print('C:     mod['+str(j)+'] below:')
                    print('-:       id,                 '         + print_var(mod[j].id           , 1   ))
                    print('C:       &io,                '         + print_var(mod[j].io           , 111 ))
                    print('-:       wd_lower_tol,       '         + print_var(mod[j].wd_lower_tol , 1   ))
                    print('-:       wd_upper_tol,       '         + print_var(mod[j].wd_upper_tol , 1   ))
                    print('-:       max_nonlin_it,      '         + print_var(mod[j].max_nonlin_it, 1   ))
                    print('C:       &solver_info,       '         + print_var(info[0]             , 111 ))
                    print('-:         max_lin_it,       '         + print_var(info[0].max_lin_it  , 1   ))
                    print('C:         &profile,         '         + print_var(info[0].profile     , 111 ))
                    print('-:           size,           '         + print_var(info[0].profile.size, 1   ))
                    print('C:           &rows,          '         + print_var(info[0].profile.rows, 111 ))
                    print('C:           &cols,          '         + print_var(info[0].profile.cols, 111 ))
                    print('-:       tau_pg,             '         + print_var(mod[j].tau_pg       , 1   ))

        for i in range(nsupmod.value):
            for j in range(sm[i].nsubmodels):
                self.assertEqual(sm[i].nsubmodels                          , 1)
                self.assertEqual(sm[i].submodel[j].id                      , adhdefine.UNSET_INT)
                self.assertEqual(sm[i].submodel[j].wd_lower_tol            ,  -0.1)
                self.assertEqual(sm[i].submodel[j].wd_upper_tol            ,   0.1)
                self.assertEqual(sm[i].submodel[j].max_nonlin_it           ,  10)
                self.assertEqual(sm[i].submodel[j].solver_info.max_lin_it  , 100)
                self.assertEqual(sm[i].submodel[j].solver_info.profile.size,   0)
                self.assertEqual(sm[i].submodel[j].tau_pg                  ,   0.5)

#------------------------------------------------------------------------------#
    def check_values_gssha(self, mvs):
        # Analyze values and addresses!
        if (gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0):
            print('Python object:', mvs)
            print('P: id(mvs),                   '+ print_var(mvs                           , 1111))
            print('C: &mvs[0],                   '+ print_var(mvs[0]                        , 111 ))
            print('-:   tmp,                     '+ print_var(mvs[0].tmp                    , 1   ))
            print('-:   yes_gtfsm,               '+ print_var(mvs[0].yes_gtfsm              , 1   ))
            print('-:   dummy_python_int,        '+ print_var(mvs[0].dummy_python_int       , 1   ))
            print('P:   dummy_python_pointer_1,  '+ print_var(mvs[0].dummy_python_pointer_1 , 333 ))
            print('P:   dummy_python_pointer_2,  '+ print_var(mvs[0].dummy_python_pointer_2 , 333 ))
            print('P:   dummy_python_pointer_3,  '+ print_var(mvs[0].dummy_python_pointer_3 , 333 ))
            print('P:   dummy_python_pointer_4,  '+ print_var(mvs[0].dummy_python_pointer_4 , 333 ))
            print('P:   dummy_python_pointer_5,  '+ print_var(mvs[0].dummy_python_pointer_5 , 333 ))
            print('P:   dummy_python_pointer_6,  '+ print_var(mvs[0].dummy_python_pointer_6 , 333 ))
            print('P:   dummy_python_pointer_7,  '+ print_var(mvs[0].dummy_python_pointer_7 , 333 ))
            print('P:   dummy_python_pointer_8,  '+ print_var(mvs[0].dummy_python_pointer_8 , 333 ))
            print('P:   dummy_python_pointer_9,  '+ print_var(mvs[0].dummy_python_pointer_9 , 333 ))
            print('P:   dummy_python_pointer_10, '+ print_var(mvs[0].dummy_python_pointer_11 , 333 ))
            print('P:   dummy_python_pointer_11, '+ print_var(mvs[0].dummy_python_pointer_10 , 333 ))
            print('P:   dummy_python_pointer_12, '+ print_var(mvs[0].dummy_python_pointer_12, 333 ))
            self.assertEqual(mvs[0].dummy_python_int, 303756215)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_1.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_2.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_3.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_4.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_5.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_6.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_7.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_8.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_9.contents ),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_10.contents),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_11.contents),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)
            self.assertEqual(ct.addressof(mvs[0].dummy_python_pointer_12.contents),ct.addressof(mvs[0])+main_var_struct.dummy_python_int.offset)


#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
    def test_main(self):

        mod = ct.POINTER(SMODEL)() # Alias, similar to SMODEL *mod; in C
        mod.contents = self.supmod[0].submodel[0] # similar to mod = &(sm[i].submodel[0]); in C

        print("\n\nBegin Controlling time!\n")
        controltimecount = 3
        mod[0].t_final = mod[0].t_final/float(controltimecount)
        adhsuper_dt = mod[0].t_final
        adhdt_original = mod[0].dt

        self.mvs[0].niter = self.mvs[0].niter//controltimecount # Integer div.
        gsshasuper_niter = self.mvs[0].niter
        gsshadt_original = self.mvs[0].dt

        for i in range(controltimecount):
            if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0:
                print('\n\nRunning AdH: controltimecount =', i+1 ,'\n')
            #Arg doesn't matter here because it is only used in CSTORM
            ierr_code = adh_run_func_(self.supmod, self.supifc,
                    self.nsupmod, self.nsupifc, byref(self.run_time))
            self.assertEqual(ierr_code, 0)

            if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0:
                print("dt            =", mod[0].dt)
                print("old_dt        =", mod[0].old_dt)
                print("t_init        =", mod[0].t_init)
                print("t_prev        =", mod[0].t_prev)
                print("t_final       =", mod[0].t_final)

            self.supmod[0].submodel[0].dt = adhdt_original
            self.supmod[0].submodel[0].t_final += adhsuper_dt

            if adhopts._DEBUG == adhdefine.ON and DEBUG_LOCAL != 0:
                print("t_final (new) =", mod[0].t_final)

            #----------------------------------------------------#
            if gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0:
                print('\n\nRunning GSSHA: controltimecount =', i+1 ,'\n')
            ierr_code = main_gssha_run(self.mvs)
            if gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0:
                print("dt            =", self.mvs[0].dt)
                print("t_curr        =", self.mvs[0].timer)
                print("t_final       =", self.mvs[0].niter)
                print("single_event_end =", self.mvs[0].single_event_end)
            #self.mvs[0].dt = gsshadt_original
            self.mvs[0].niter += gsshasuper_niter
            self.mvs[0].go = gsshatypes.TRUE
            self.mvs[0].single_event_end += float(gsshasuper_niter)/1440.0
            if gsshaopts._DEBUG == gsshadefine.ON and DEBUG_LOCAL != 0:
                print("t_final (new) =", self.mvs[0].niter)


#------------------------------------------------------------------------------#
if __name__ == '__main__':
    unittest.main()
