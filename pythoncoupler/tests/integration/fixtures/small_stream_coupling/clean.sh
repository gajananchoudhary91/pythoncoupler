rm Stream.cdp \
   Stream.cdq \
   Stream.dep \
   Stream.drn \
   Stream.ohl \
   Stream.sum \
   Stream.otl \
   bad_thalweg.out \
   short_links.out \
   maskmap \
   small_2d*.h5_back \
   small_2d*.xmf_back \
   small_2d*.h5 \
   small_2d*.xmf \
   small_2d*.dat \
   *.dat

