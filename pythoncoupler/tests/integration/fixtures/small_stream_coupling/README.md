# Running coupled AdH-GSSHA model:

* pip-install `adhpython` and `gsshapython`, which are prerquisites for
  pythoncoupler.
* For Serial simulation (and `adhpython` installation), run the following
  command on the terminal:
    ```bash
    python -m pythoncoupler   2   <couplingtype>   Stream.prj   small_2d
    ```
  and for parallel runs (and `adhpython` installation), run:
    ```bash
        mpirun   -np   1   python -m pythoncoupler   2  <couplingtype>   Stream.prj   small_2d
    ```
  where <couplingtype> is one of `gda`, `adg`, `gdadg`, `adgda`.
