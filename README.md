# Python coupler for AdH and GSSHA

This project is for coupling Adaptive Hydraulics (AdH) and Gridded Surface and Subsurface Hydrologic Analysis (GSSHA), 
which are two software developed at the Engineering Research and Development Center, Vicksburg, Mississippi, USA. 
It is mainly a python interface that makes use of the AdH and GSSHA python interfaces to couple the two software, 
with only a few changes to AdH and GSSHA source codes. The code is intended to run in serial as well as parallel.


## Getting Started

### Prerequisites

* Python 2x or 3x
* AdH shared library     : `libadh.so`
* AdH python interface   : `adhpython`
* GSSHA shared library   : `libgssha.so`
* GSSHA python interface : `gsshapython`
* Operating system       : `Linux` (MacOS/Windows have not been tested)

If you must also modify the code, knowledge of Python's `ctypes` is additionally
needed.

### Installing `pythoncoupler`

After installing the prerequisite AdH and GSSHA Python interfaces by
pip-installing them, clone this repository, change the current directory to
`<repo_root>` and pip-install `pythoncoupler` by running:
```bash
python -m pip install .
```
To uninstall `pythoncoupler`, run:
```bash
python -m pip uninstall pythoncoupler
```

## Testing `pythoncoupler`

Currently, no automated tests have been enabled for `pythoncoupler`; testing
must be done manually.

## Using `pythoncoupler`

On pip-installing `pythoncoupler`, you can run coupled simulations directly on
the command line by running the following command on the terminal,
`python -m pythoncoupler <coupled_AdH_edgestring> <couplingtype> <GSSHA_file> <AdH_file>`,
where `<coupeld_AdH_edgestring>` is 1-based, `<couplingtype>` is one of the
one-way coupling types, `adg` and `gda`, or two-way coupling types, `gdadg` and
`adgda`, `<GSSHA_file>` is the input project file name *with* extension,
`<AdH_file>` is the AdH model's input file name *without* extension. Note: The
coupled AdH edgestring must have a NB VEL flux boundary condition with at least
4 timestamps in the corresponding boundary series.

For example, say you want to run a coupled AdH-GSSHA simulation with AdH input
files named `small_2d*` and GSSHA files named `Stream*`, including the project
file `Stream.prj`. Say edgestring 3 of the AdH model is coupled with the 1-D
stream outlet of the GSSHA watershed model. In order to run two-way coupling
with GSSHA staying ahead of AdH, run the following in the terminal:

```bash
mkdir mycoupledsimulation
cd mycoupledsimulation
cp <path_to_adh_input_files>/small_2d* .
cp <path_to_gssha_input_files>/Stream* .
python -m pythoncoupler 3 gdadg Stream.prj small_2d
```

For parallel runs when AdH and GSSHA have been compiled with gcc, use
```
mpirun -np <num_procs> python -m pythoncoupler 3 gdadg Stream.prj small_2d
```
where `<num_procs>` is the number of processors you want to use.


## Authors

* **Gajanan Choudhary** - [website](https://users.oden.utexas.edu/~gajanan/)

## License

Proper licensing yet to be determined. For now, authors reserve all rights for the current 
version. For anyone else, this project is currently restricted for all purposes, no copying, 
distribution, download, use, or sale is permitted, commercial or otherwise, EXCEPT by ERDC 
employees, who are allowed to download, modify, and use the software. The project is 
currently under development, the code is provided as-is with no express warranty, and the 
authors may not be held liable for any damages arising from anything related to this project. 
Licensing will be dealt with in later versions of this project.

## Acknowledgments

* `ctypes` documentation
* Stackoverflow

