.PHONY: init test clean all docs install uninstall

init:
	python setup.py build

test:
	python -m unittest tests

clean:
	rm -rf build/

all:
	#python -m pip install -r requirements.txt
	python setup.py build

docs:
	python -m pip install -r docs-requirements.txt
	sphinx-apidoc -o docs pythoncoupler

install:
	#python -m pip install -r requirements.txt
	python -m pip install .

uninstall:
	python -m pip uninstall pythoncoupler
