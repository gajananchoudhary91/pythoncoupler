#!/usr/bin/env python3
import os
from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

pythoncoupler_cmds = ['pythoncoupler = pythoncoupler.__main__:main']

setup(
    name='pythoncoupler',
    version='0.1.0',
    description='Program for coupling hydrodynamic and hydrologic software',
    keywords='Coupling, hydrologic, hydrodynamic, AdH, GSSHA',
    long_description=readme,
    author='Gajanan Choudhary',
    author_email='gajananchoudhary91@gmail.com',
    url='https://github.com/gajanan-choudhary/pythoncoupler',
    license=license,
    packages=find_packages(exclude=('tests', 'doc')),
    entry_points={'console_scripts': pythoncoupler_cmds},
    package_dir={'': '.'},
    package_data={'': []},
)
